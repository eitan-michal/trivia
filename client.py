import socket
import json
import netstruct
SERVER_PORT = 2222
SERVER_IP = "127.0.0.1"

SIGNUP_CODE = 200
LOGIN_CODE = 201


def connect_server(sock):
    """
    Function connect's to the server
    :param sock: the conversation socket
    :return: none
    """
    # Connecting to remote computer 2222
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)


def receive_response(sock):
    """
    Function returns the server's response
    :param sock: the conversation socket
    :return: the server's decoded message
    """
    server_msg = sock.recv(1024)
    return server_msg[5:].decode()


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:  # socket created
        try:
            connect_server(sock)  # connecting server
        except Exception as e:
            print("Error with server connection: ", e)
            exit(1)

        login_json = {
            'username': "Sahar",
            'password': "1234",
            'email': 'Shimshon@gmail.com'
        }
        login_msg = json.dumps(login_json)  # Converting json to string

        var = netstruct.pack(b"B i b$", SIGNUP_CODE, len(login_msg), login_msg.encode('UTF-8'))  # Creating packet data
        sock.sendall(var)

        # Receiving response (error / status code)
        response = receive_response(sock)
        print(response)


if __name__ == '__main__':
    main()
