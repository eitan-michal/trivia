import requests
import json
import sqlite3
DB_NAME = "MyDB.sqlite"
TABLE_QUERY = "create table questions(id INTEGER NOT NULL PRIMARY KEY, question text not null, correct text not null," \
                   " answer1 text not null, answer2 text not null, answer3 text not null);"


def get_question_query(question_json):
    query = "insert into questions (question, correct, answer1, answer2, answer3) values "
    list_of_questions = question_json["results"]
    for question_dict in list_of_questions:
        query += "('" + question_dict['question'] + "', '" + question_dict["correct_answer"] + \
        "', '" + question_dict["incorrect_answers"][0] + "', '" + question_dict["incorrect_answers"][1] + "', '" + \
                 question_dict["incorrect_answers"][2] + "'), "

    return query[:-2] + ';'


def main():
    response = requests.get("https://opentdb.com/api.php?amount=10&type=multiple")
    question_json = eval(response.text)

    insert_questions_query = get_question_query(question_json)

    connection = sqlite3.connect(DB_NAME)

    cursor = connection.cursor()

    try:
        cursor.execute(TABLE_QUERY)
    except:
        print("DB exists")
    cursor.execute(insert_questions_query)

    connection.commit()
    connection.close()


if __name__ == '__main__':
    main()
