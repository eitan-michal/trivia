﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TriviaWPF
{
    /// <summary>
    /// Interaction logic for GameResultsScreen.xaml
    /// </summary>
    public partial class GameResultsScreen : Window
    {
        string username;
        Communicator communicator = new Communicator();

        public GameResultsScreen(string username)
        {
            InitializeComponent();
            this.username = username;
            GameResults();
        }

        private void GameResults()
        {
             byte[] res = { 0 };
            try
            {
                communicator.sendMsgToServer(Serializer.SerializerCodeRequest(PacketCode.GET_GAME_RESULT_REQUEST));
                res = communicator.getMsgFromServer();
            }
            catch (Exception)
            {
                TextBlockError.Text = "Server has crashed";
                return;
            }
            if (Convert.ToInt32(res[0]) == Convert.ToInt32(PacketCode.GET_GAME_RESULT_RESPONSE))
            {
                GetGameResultsResponse getGameResultsResponse = Deserializer.deserializeResponse<GetGameResultsResponse>(res);
                if (getGameResultsResponse.status == Deserializer.failureStatus)
                {
                    TextBlockError.Text = "failed to get info";
                }
                else if (getGameResultsResponse.status == Deserializer.successStatus) {
                    var objects = JArray.Parse(getGameResultsResponse.results);
                    string singleResult = "";
                    foreach (JObject Curresult in objects)
                    {
                        dynamic result = JsonConvert.DeserializeObject(Curresult.ToString());
                        singleResult = "name: " + result.username + " | Score: " + result.correctAnswerCount + " | time: " + result.averageAnswerTime;
                        GameResultsListBox.Items.Add(singleResult);
                    }
                }
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void LeaveButton_Click(object sender, RoutedEventArgs e)
        {
            byte[] res = { 0 };
            try
            {
                communicator.sendMsgToServer(Serializer.SerializerCodeRequest(PacketCode.LEAVE_GAME_REQUEST));
                res = communicator.getMsgFromServer();
            }
            catch (Exception)
            {
                MessageBox.Show("Server has crashed");
            }

            if (Convert.ToInt32(res[0]) == Convert.ToInt32(PacketCode.LEAVE_GAME_RESPONSE))
            {
                LeaveGameResponse leaveGameResponse = Deserializer.deserializeResponse<LeaveGameResponse>(res);
                if (leaveGameResponse.status == Deserializer.failureStatus)
                {
                    MessageBox.Show("Could not get the scores properly");
                }
            }

            MenuScreen menuScreen = new MenuScreen(username);
            menuScreen.Show();
            this.Close();

            
        }
    }
}
