﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TriviaWPF
{
    /// <summary>
    /// Interaction logic for BestScoresScreen.xaml
    /// </summary>
    public partial class BestScoresScreen : Window
    {
        private string username;

        public BestScoresScreen(string user)
        {
            InitializeComponent();
            username = user;
            TextBlockUser.Text = user;

            updateScores();
        }

        private void updateScores()
        {
            Communicator communicator = new Communicator();
            Byte[] msg = { 0 };
            try  // Sending request
            {
                communicator.sendMsgToServer(Serializer.SerializerCodeRequest(PacketCode.GET_HIGH_SCORE_REQUEST));
                msg = communicator.getMsgFromServer();
            }
            catch (Exception)
            {
                TextBlockErrorMsg.Text = "Server has crashed";
                return;
            }

            if (Convert.ToInt32(msg[0]) == Convert.ToInt32(PacketCode.GET_HIGH_SCORE_RESPONSE))
            {
                GetHighScoreResponse getPersonalStatsResponse = Deserializer.deserializeResponse<GetHighScoreResponse>(msg);

                if (getPersonalStatsResponse.status == Deserializer.failureStatus)
                {
                    TextBlockErrorMsg.Text = "Could not get the scores";
                    return;
                }

                dynamic info = JsonConvert.DeserializeObject(getPersonalStatsResponse.UserStatistics);  // saving stats dynamicly (and removing many '/' from string)
                var objects = JArray.Parse(info);  // Parsing a list of data (list -> [1, 2, ..., n]) into a number of objects

                List<string> scores = new List<string>();

                foreach (JObject curUser in objects)  // Going through each object in the list
                {
                    dynamic newUser = JsonConvert.DeserializeObject(curUser.ToString());
                    string name = newUser.name;
                    int score = newUser.score;

                    scores.Add(name + " - " + score.ToString());
                }

                // Adding data to be presented on GUI
                TextBlock[] textBlocks = { TextBlockUser1, TextBlockUser2, TextBlockUser3 };
                for (int i = 0; i < scores.Count(); i++)
                {
                    textBlocks[i].Text = scores[i];
                }
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            MenuScreen menuScreen = new MenuScreen(username);
            menuScreen.Show();
            this.Close();
        }
    }
}
