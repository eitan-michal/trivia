﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaWPF
{
    public partial class MenuScreen : Window
    {
        Communicator communicator = new Communicator();
        private string username;

        public MenuScreen(string name)
        {
            InitializeComponent();
            username = name;
            TextBlockHelloUser.Text = "Hello, " + username;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private bool logout()
        {
            byte[] msg = { 0 };
            try
            {
                communicator.sendMsgToServer(Serializer.SerializerCodeRequest(PacketCode.LOGOUT_REQUEST));
                msg = communicator.getMsgFromServer();
            }
            catch (Exception)
            {
                TextBlockError.Text = "The server has crashed";
            }

            return (Convert.ToInt32(msg[0]) == Convert.ToInt32(PacketCode.LOGOUT_REQUEST));
        }

        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {

            if (logout())
            {
                MainWindow mainWindow = new MainWindow(false);
                mainWindow.Show();
                this.Close();
            }
            else
            {
                TextBlockError.Text = "Logout failed";
            }
        }

        public void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            logout();
            Application.Current.Shutdown();
        }

        public void MyStatsButton_Click(object sender, RoutedEventArgs e)
        {
            StatsScreen statsScreen = new StatsScreen(username);
            statsScreen.Show();
            this.Close();
        }

        public void BestScoresButton_Click(object sender, RoutedEventArgs e)
        {
            BestScoresScreen bestScoresScreen = new BestScoresScreen(username);
            bestScoresScreen.Show(); 
            this.Close();
        }

        public void CreateRoomButton_Click(object sender, RoutedEventArgs e)
        {
            CreateRoomScreen createRoomScreen = new CreateRoomScreen(username);
            createRoomScreen.Show();
            this.Close();
        }

        public void JoinRoomButton_Click(object sender, RoutedEventArgs e)
        {
            JoinRoomScreen joinRoomScreen = new JoinRoomScreen(username);
            joinRoomScreen.Show();
            this.Close();
        }
        
    }
}
