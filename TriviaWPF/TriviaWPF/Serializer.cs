﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace TriviaWPF
{
    enum PacketCode
    {
        LOGIN_REQUEST = 10,
        LOGIN_RESPONSE = 11,
        SIGNUP_REQUEST = 20,
        SIGNUP_RESPONSE = 21,
        LOGOUT_REQUEST = 30,
        LOGOUT_RESPONSE = 31,
        HIGH_SCORE_REQUEST = 40,
        HIGH_SCORE_RESPONSE = 41,
        GET_ROOMS_REQUEST = 50,
        GET_ROOMS_RESPONSE = 51,
        GET_HIGH_SCORE_RESPONSE = 60,
        GET_HIGH_SCORE_REQUEST = 61,
        GET_ROOM_RESPONSE = 70,
        GET_ROOM_REQUEST = 71,
        CREATE_ROOM_RESPONSE = 80,
        CREATE_ROOM_REQUEST = 81,
        JOIN_ROOM_RESPONSE = 90,
        JOIN_ROOM_REQUEST = 91,
        GET_PLAYERS_IN_ROOM_RESPONSE = 100,
        GET_PLAYERS_IN_ROOM_REQUEST = 101,
        GET_PERSONAL_STATS_RESPONSE = 110,
        GET_PERSONAL_STATS_REQUEST = 111,
        CLOSE_ROOM_RESPONE = 120,
        CLOSE_ROOM_REQUEST = 121,
        START_GAME_RESPONSE = 130,
        START_GAME_REQUEST = 131,
        GET_ROOM_STATE_RESPONSE = 140,
        GET_ROOM_STATE_REQUEST = 141,
        LEAVE_ROOM_RESPONSE = 150,
        LEAVE_ROOM_REQUEST = 151,
        LEAVE_GAME_RESPONSE = 160,
        LEAVE_GAME_REQUEST = 161,
        GET_QUESTION_RESPONSE = 170,
        GET_QUESTION_REQUEST = 171,
        SUBMIT_ANSWER_RESPONSE = 180,
        SUBMIT_ANSWER_REQUEST = 181,
        GET_GAME_RESULT_RESPONSE = 190,
        GET_GAME_RESULT_REQUEST = 191,
        ERROR_RESPONSE = 200
    };
    struct LoginRequest
    {
        public string username;
        public string password;
    }
    struct SignupRequest
    {
        public string username;
        public string password;
        public string email;
    }
    struct GetPlayersInRoomRequest
    {
        public int roomid;
    }
    struct JoinRoomRequest
    {
        public int roomid;
    }
    public struct CreateRoomRequest
    {
        public string roomName;
        public int maxUsers;
        public int qustionCount;
        public int answerTimeout;
    }

    public struct SubmitAnswerRequest
    {
        public int answerId;
        public int time;
    }

    class Serializer
    {

        private static Byte[] SerializeRequest(PacketCode code, string data)
        {
            byte[] result = new byte[5 + data.Length];
            byte[] length = BitConverter.GetBytes(data.Length);
            result[0] = Convert.ToByte(code);
            length.CopyTo(result, 1);
            Encoding.ASCII.GetBytes(data).CopyTo(result, length.Length + 1);

            return result;
        }

        static public Byte[] SerializerCodeRequest(PacketCode code)
        {
            return SerializeRequest(code, "");
        }

        static public Byte[] SerializerLoginRequest(LoginRequest loginRequest)
        {
            return SerializeRequest(PacketCode.LOGIN_REQUEST, JsonConvert.SerializeObject(loginRequest, Formatting.Indented));
        }

        static public Byte[] SerializerSignUpRequest(SignupRequest signupRequest)
        {
            return SerializeRequest(PacketCode.SIGNUP_REQUEST, JsonConvert.SerializeObject(signupRequest, Formatting.Indented));
        }

        public static Byte[] SerializerGetPlayersInRoomRequest(GetPlayersInRoomRequest getPlayersInRoomRequest)
        {
            return SerializeRequest(PacketCode.GET_PLAYERS_IN_ROOM_REQUEST, JsonConvert.SerializeObject(getPlayersInRoomRequest, Formatting.Indented));
        }

        public static Byte[] SerializerJoinRoomRequest(JoinRoomRequest joinRoomRequest)
        {
            return SerializeRequest(PacketCode.JOIN_ROOM_REQUEST, JsonConvert.SerializeObject(joinRoomRequest, Formatting.Indented));
        }

        public static Byte[] SerializerCreateRoomRequest(CreateRoomRequest createRoomRequest)
        {
            return SerializeRequest(PacketCode.CREATE_ROOM_REQUEST, JsonConvert.SerializeObject(createRoomRequest, Formatting.Indented));
        }

        public static Byte[] SerializeSubmitAnswerRequest(SubmitAnswerRequest submitAnswerRequest)
        {
            return SerializeRequest(PacketCode.SUBMIT_ANSWER_REQUEST, JsonConvert.SerializeObject(submitAnswerRequest, Formatting.Indented));
        }
    }
}