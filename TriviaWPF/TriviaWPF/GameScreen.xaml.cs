﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace TriviaWPF
{
    /// <summary>
    /// Interaction logic for GameScreen.xaml
    /// </summary>
    public partial class GameScreen : Window
    {
        private string username;
        private int numOfQuestions;
        private int timePerQuestion;
        private int correctAnswers = 0;

        private DispatcherTimer timer;
        private int counter = 0;
        private List<dynamic> answers = new List<dynamic>();

        private Communicator communicator = new Communicator();
        private bool handleSelected = false;

        private const int nonSelected = 9;

        public GameScreen(string name, int questionsCount, int time)
        {
            InitializeComponent();
            username = name;
            numOfQuestions = questionsCount;
            timePerQuestion = time;
            counter = time;


            TextBlockUser.Text = name;
            TextBlockTime.Text = time.ToString();
            TextBlockScore.Text = "0";
            TextBlockQuestionsLeft.Text = numOfQuestions.ToString();

            timer = new DispatcherTimer(); // creating a new timer
            timer.Interval = TimeSpan.FromMilliseconds(1000); // this timer will trigger every 1000 milliseconds (1 second)
            timer.Tick += _timer_Tick;
            timer.Start(); // starting the timer
            updateQuestion();  // updating first question
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            if (counter < 1)
            {
                counter = timePerQuestion;
                numOfQuestions--;
                TextBlockQuestionsLeft.Text = numOfQuestions.ToString();

                if (numOfQuestions == 0)  // When the game is finished
                {
                    timer.Stop();
                    handleSelected = false;
                    GameResultsScreen gameResultsScreen = new GameResultsScreen(username);
                    gameResultsScreen.Show();
                    this.Close();
                }
                else
                {
                    if (AnwersListBox.SelectedItem == null)  // nothing was selected
                    {
                        SubmitAnswer(nonSelected, timePerQuestion);
                    }
                    updateQuestion();
                }
            }
            else
            {
                counter--;
            }

            TextBlockTime.Text = counter.ToString();
        }


        private void updateQuestion()
        {
            byte[] getQuestionMsg = Serializer.SerializerCodeRequest(PacketCode.GET_QUESTION_REQUEST);
            byte[] res = { 0 };

            handleSelected = false;
            AnwersListBox.SelectedIndex = -1;  // deselecting

            AnwersListBox.ClearValue(ItemsControl.ItemsSourceProperty);
            AnwersListBox.Items.Clear();
            answers.Clear();

            try
            {
                communicator.sendMsgToServer(getQuestionMsg);
                res = communicator.getMsgFromServer();
            }
            catch (Exception )
            {
                TextBlockError.Text = "Server has crashed";
            }

            if (Convert.ToInt32(res[0]) == Convert.ToInt32(PacketCode.GET_QUESTION_RESPONSE))
            {
                GetQuestionReponse getQuestionReponse = Deserializer.deserializeResponse<GetQuestionReponse>(res);
                if (getQuestionReponse.status == Deserializer.failureStatus)
                {
                    TextBlockError.Text = "theres no question";
                    return;
                }
                else if (getQuestionReponse.status == Deserializer.successStatus) 
                {
                    TextBlockQuestion.Text = getQuestionReponse.question;

                    var objects = JArray.Parse(getQuestionReponse.answers);  // make a list 

                    foreach (JObject curAnswer in objects)  // run on list 
                    {
                        dynamic newAnswer = JsonConvert.DeserializeObject(curAnswer.ToString());
                        answers.Add(newAnswer);
                    }

                    AnwersListBox.ItemsSource = answers;
                    AnwersListBox.DisplayMemberPath = "answer";  // showing only the answer field
                    AnwersListBox.IsEnabled = true;
                }
                handleSelected = true;
            }
        }

        private void AnwersListBox_SelectedIndexChanged(object sender, SelectionChangedEventArgs e)
        {
            byte[] res = { 0 };
            int ansId = 9;

            if (!handleSelected) { return; }

            if (AnwersListBox.SelectedItem != null)
            {
                dynamic ans = AnwersListBox.SelectedItem;
                ansId = ans.id;

            }
            SubmitAnswer(ansId, timePerQuestion - counter);
        }

        private void SubmitAnswer(int answerID, int AnswerTime)
        {
            byte[] res = { 0 };

            SubmitAnswerRequest submitAnswerRequest = new SubmitAnswerRequest { answerId = answerID, time = AnswerTime };
            try
            {
                communicator.sendMsgToServer(Serializer.SerializeSubmitAnswerRequest(submitAnswerRequest));
                res = communicator.getMsgFromServer();
            }
            catch (Exception)
            {
                TextBlockError.Text = "Server has crashed";
            }
            if (Convert.ToInt32(res[0]) == Convert.ToInt32(PacketCode.SUBMIT_ANSWER_RESPONSE))
            {
                SubmitAnswerResponse submitAnswerResponse = Deserializer.deserializeResponse<SubmitAnswerResponse>(res);
                if (submitAnswerResponse.status == Deserializer.failureStatus)
                {
                    return;
                }
                else if (submitAnswerResponse.status == Deserializer.successStatus)
                {
                    if (submitAnswerResponse.correctAnswerId == answerID - 1)
                    {
                        correctAnswers++;
                        TextBlockScore.Text = correctAnswers.ToString();
                    }
                }
            }
            AnwersListBox.IsEnabled = false;
        }


        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void LeaveButton_Click(object sender, RoutedEventArgs e)
        {
            byte[] res = { 0 };
            timer.Stop();
            try
            {
                communicator.sendMsgToServer(Serializer.SerializerCodeRequest(PacketCode.LEAVE_GAME_REQUEST));
                res = communicator.getMsgFromServer();
            }
            catch (Exception) {
                MessageBox.Show("Server has crashed");
            }

            if (Convert.ToInt32(res[0]) == Convert.ToInt32(PacketCode.LEAVE_GAME_RESPONSE))
            {
                LeaveGameResponse leaveGameResponse = Deserializer.deserializeResponse<LeaveGameResponse>(res);
                if (leaveGameResponse.status == Deserializer.failureStatus)
                {
                    MessageBox.Show("Could not leave the game properly");
                }
            }
            MenuScreen menuScreen = new MenuScreen(username);
            menuScreen.Show();
            this.Close();
        }
    }
}
