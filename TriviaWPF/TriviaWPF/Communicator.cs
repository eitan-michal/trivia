﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace TriviaWPF
{
    class Communicator
    {
        private static TcpClient client; 
        private static NetworkStream clientStream;

        public Communicator()
        {
        }
        public Communicator(string iPAddress, int port)
        {
            client = new TcpClient(); // create new client 
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(iPAddress), port); // object with port and iPAddress
            client.Connect(serverEndPoint); // connect client to server 
            clientStream = client.GetStream(); 
        }

        public bool isInitialized()
        {
            return clientStream != null;
        }

        public void closeConnection()
        {
            client.Close();
            clientStream.Close();
        }

        public Byte[] getMsgFromServer()
        {
            Byte[] data = new Byte[1024];
            clientStream.Read(data, 0, 1024);
            return data;
        }
        public void sendMsgToServer(byte[] data) {
            clientStream.Write(data, 0, data.Length);
            clientStream.Flush();
        }
    }
}
