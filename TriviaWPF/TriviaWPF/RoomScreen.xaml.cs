﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;

namespace TriviaWPF
{
    /// <summary>
    /// Interaction logic for RoomScreen.xaml
    /// </summary>
    public partial class RoomScreen : Window
    {
        private string username;
        private RoomData roomData;

        private Communicator communicator = new Communicator();
        private Thread updateThread;

        public RoomScreen(string name, RoomData roomData, bool isAdmin)
        {
            InitializeComponent();

            username = name;
            TextBlockUser.Text = name;

            TextBlockRoomName.Text = roomData.name;
            TextBlockMaxPlayers.Text += roomData.maxPlayers;
            TextBlockNumOfQuestions.Text += roomData.numOfQuestionsInGame;
            TextBlockTimePerQuestion.Text += roomData.timePerQuestion;

            this.roomData = roomData;

            if (!isAdmin)
            {
                ButtonStartGame.Visibility = Visibility.Collapsed;
                ButtonCloseRoom.Visibility = Visibility.Collapsed;
            }
            else {
                ButtonLeaveRoom.Visibility = Visibility.Collapsed;
            }

            updateThread = new Thread(updateRoomState);
            updateThread.Start();
            
        }

        private void updateRoomState()
        {
            Byte[] res = { 0 }, msg = Serializer.SerializerCodeRequest(PacketCode.GET_ROOM_STATE_REQUEST);

            while (true) 
            {
                this.Dispatcher.Invoke(() =>    // Clearing players list in GUI
                {
                    UsersListBox.ClearValue(ItemsControl.ItemsSourceProperty);
                    UsersListBox.Items.Clear();
                });

                try
                {
                    communicator.sendMsgToServer(msg);
                    res = communicator.getMsgFromServer();
                }
                catch (ThreadAbortException ex)
                {
                    throw ex;
                }
                catch (Exception)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        TextBlockError.Text = "Server has crashed";
                    });
                }

                if (Convert.ToInt32(res[0]) == Convert.ToInt32(PacketCode.GET_ROOM_STATE_RESPONSE))
                {
                    GetRoomStateResponse getRoomStateResponse = Deserializer.deserializeResponse<GetRoomStateResponse>(res);

                    if (getRoomStateResponse.status == Deserializer.failureStatus)
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            TextBlockError.Text = "The room has been closed";
                            leaveRoom();
                        });
                    }
                    else if (getRoomStateResponse.players == "") 
                    {
                        Thread.Sleep(3000);
                        continue;
                    }
                    else if (getRoomStateResponse.hasGameBegun == false) 
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            try
                            {
                                updateThread.Abort();
                            }
                            catch (Exception) { }

                            GameScreen gameScreen = new GameScreen(username, roomData.numOfQuestionsInGame, roomData.timePerQuestion);
                            gameScreen.Show();
                            this.Close();
                        });
                    }


                    string strPlayers = getRoomStateResponse.players.Substring(1, getRoomStateResponse.players.Length - 2);

                    this.Dispatcher.Invoke(() =>
                    {
                        UsersListBox.ItemsSource = strPlayers.Split(new char[] { ',' }).ToList();
                    });

                }
                else {
                    this.Dispatcher.Invoke(() =>
                    {
                        TextBlockError.Text = "Error: problem with server communication";
                    });
                }
                Thread.Sleep(3000);
            }
        }

        private void leaveRoom()
        {
            try
            {
                updateThread.Abort();
            }
            catch (Exception) { }

            MenuScreen menuScreen = new MenuScreen(username);
            menuScreen.Show();
            this.Close();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
        
        private void ButtonLeaveRoom_Click(object sender, RoutedEventArgs e)
        {
            Byte[] msg = { 0 };
            try
            {
                communicator.sendMsgToServer(Serializer.SerializerCodeRequest(PacketCode.LEAVE_ROOM_REQUEST));
                msg = communicator.getMsgFromServer();
            }
            catch (Exception)
            {
                MessageBox.Show("Server has crashed");
                leaveRoom();
            }
            if (Convert.ToInt32(msg[0]) == Convert.ToInt32(PacketCode.LEAVE_ROOM_RESPONSE)) {
                LeaveRoomResponse leaveRoomResponse = Deserializer.deserializeResponse<LeaveRoomResponse>(msg);
                if (leaveRoomResponse.status == Deserializer.failureStatus) {
                    MessageBox.Show("Error: Didn't leave the room properly.");
                }

                leaveRoom();
            }

        }

        private void ButtonStartGame_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                communicator.sendMsgToServer(Serializer.SerializerCodeRequest(PacketCode.START_GAME_REQUEST));
                Byte[] msg = communicator.getMsgFromServer();
                updateThread.Abort();
            }
            catch (ThreadAbortException) { }
            catch (Exception) {
                TextBlockError.Text = "Server has crashed";
                return;
            }

            GameScreen gameScreen = new GameScreen(username, roomData.numOfQuestionsInGame, roomData.timePerQuestion);
            gameScreen.Show();
            this.Close();
        }

        private void ButtonCloseRoom_Click(object sender, RoutedEventArgs e)
        {
            Byte[] msg = { 0 };

            try
            {
                communicator.sendMsgToServer(Serializer.SerializerCodeRequest(PacketCode.CLOSE_ROOM_REQUEST));
                msg = communicator.getMsgFromServer();
            }
            catch (Exception)
            {
                MessageBox.Show("Server has crashed");
                leaveRoom();
            }

            if (Convert.ToInt32(msg[0]) == Convert.ToInt32(PacketCode.CLOSE_ROOM_RESPONE))
            {
                CloseRoomRespone closeRoomRespone = Deserializer.deserializeResponse<CloseRoomRespone>(msg);

                if (closeRoomRespone.status == Deserializer.failureStatus)
                {
                    MessageBox.Show("Error: could not close the room properly.");
                }

                leaveRoom();
            }
            else
            {
                TextBlockError.Text = "Error: could not close the room";
                return;
            }
            

        }

    }
}
