﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;

namespace TriviaWPF
{
    public partial class MainWindow : Window
    {

        private Communicator communicator = new Communicator();

        public MainWindow(bool isInitialized)
        {
            InitializeComponent();
            TextBlockError.Text = "";

            if (isInitialized)
                return;

            tryAgain:
            try
            {
                communicator = new Communicator("127.0.0.1", 2222);  // Creating a new connection with server
            }
            catch (Exception)
            {
                // Trying to connect again
                MessageBoxResult result = MessageBox.Show("Could not connect to the server.\nWould you like to try again?",
                                          "Error",
                                          MessageBoxButton.YesNo,
                                          MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    goto tryAgain;
                }
                else
                {
                    Application.Current.Shutdown();
                }

            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            Byte[] msg = { 0 };
            string name = TextBoxUserName.Text;
            string password = TextBoxPassword.Password;

            if (name.Length == 0 || password.Length == 0)
            {
                TextBlockError.Text = "Insert user name and password!";
                return;
            }

            try  // sending a login request and receiving response
            {
                LoginRequest loginRequest = new LoginRequest { username = name, password = password };
                communicator.sendMsgToServer(Serializer.SerializerLoginRequest(loginRequest));
                msg = communicator.getMsgFromServer();
            }
            catch (Exception)
            {
                MessageBox.Show("Server has crashed");
            }


            if (Convert.ToInt32(msg[0]) == Convert.ToInt32(PacketCode.LOGIN_RESPONSE))
            {
                LoginResponse loginResponse = Deserializer.deserializeResponse<LoginResponse>(msg);
                if (loginResponse.status == Deserializer.failureStatus)
                {
                    TextBlockError.Text = "Username or Password are invalid!";
                    return;
                }
                else if (loginResponse.status == Deserializer.alreadyLogged)
                {
                    TextBlockError.Text = "User is already logged";
                    return;
                }
                else {
                    MenuScreen menuScreen = new MenuScreen(name);
                    menuScreen.Show();
                    this.Close();
                }
            }
            else {
                ErrorResponse errorResponse = Deserializer.deserializeResponse<ErrorResponse>(msg);
                TextBlockError.Text = "An error has accured!";
            }

        }

        private void SignupScreenButton_Click(object sender, RoutedEventArgs e)
        {
            SignupScreen signupScreen = new SignupScreen();
            signupScreen.Show();
            this.Close();
        }

        private void ExitButton_click(object sender, RoutedEventArgs e) 
        {
            Application.Current.Shutdown();
        }
    }
}
