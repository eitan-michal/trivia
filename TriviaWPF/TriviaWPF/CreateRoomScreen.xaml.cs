﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace TriviaWPF
{
    /// <summary>
    /// Interaction logic for CreateRoomScreen.xaml
    /// </summary>
    public partial class CreateRoomScreen : Window
    {
        private string username;

        public CreateRoomScreen(string name)
        {
            InitializeComponent();

            TextBlockUser.Text = name;
            this.username = name;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        // Events called on textbox input
        // checking if input is valid
        private void TextBox_PreviewTextInputPlayers(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text, 1, 5);
        }
        private void TextBox_PreviewTextInputQuestions(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text, 0, 10);
        }
        private void TextBox_PreviewTextInputTime(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text, 0, 30);
        }

        public static bool IsValid(string str, int min, int max)
        {
            int i;
            return int.TryParse(str, out i) && i >= min && i <= max;  // testing if string is a number && min<=i<=max
        }

        public void BackButton_Click(object sender, RoutedEventArgs e)
        {
            MenuScreen menuScreen = new MenuScreen(username);
            menuScreen.Show();
            this.Close();
        }
        
        public void SendButton_Click(object sender, RoutedEventArgs e)
        {
            Communicator communicator = new Communicator();
            byte[] res;


            if (TextBoxRoomName.Text.Length == 0 || TextBoxNumOfQuestions.Text.Length == 0 || TextBoxNumOfQuestions.Text.Length == 0 || TextBoxTimeForQuestion.Text.Length == 0)
            {
                TextBlockError.Text = "Fill all the fields";
                return;
            }

            string roomName = TextBoxRoomName.Text;
            int numOfPlayers = int.Parse(TextBoxNumOfPlayers.Text);
            int numOfQuestions = int.Parse(TextBoxNumOfQuestions.Text);
            int timePerQuestion = int.Parse(TextBoxTimeForQuestion.Text);

            if (numOfQuestions < 2 || timePerQuestion < 5)
            {
                TextBlockError.Text = "Values are not in range";
                return;
            }

            CreateRoomRequest createRoomRequest = new CreateRoomRequest { roomName = roomName, maxUsers = numOfPlayers, qustionCount = numOfQuestions, answerTimeout = timePerQuestion };

            try
            {
                communicator.sendMsgToServer(Serializer.SerializerCreateRoomRequest(createRoomRequest));
                res = communicator.getMsgFromServer();
            }
            catch (Exception)
            {
                TextBlockError.Text = "Server has crashed";
                return;
            }

            if (Convert.ToInt32(res[0]) == Convert.ToInt32(PacketCode.CREATE_ROOM_RESPONSE))
            {
                CreateRoomResponse createRoomResponse = Deserializer.deserializeResponse<CreateRoomResponse>(res);

                if (createRoomResponse.status == Deserializer.failureStatus)
                {
                    TextBlockError.Text = "Could not create a room :(";
                    return;
                }
                else if (createRoomResponse.status == Deserializer.successStatus) {
                    RoomData roomData = new RoomData { id = 1, name = roomName, maxPlayers = numOfPlayers, numOfQuestionsInGame = numOfQuestions, timePerQuestion = timePerQuestion, isActive = 1 };
                    RoomScreen roomScreen = new RoomScreen(username, roomData, true);
                    roomScreen.Show();
                    this.Close();
                }
            }
        }
    }
}
