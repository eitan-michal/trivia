﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TriviaWPF
{
    /// <summary>
    /// Interaction logic for JoinRoomScreen.xaml
    /// </summary>
    public partial class JoinRoomScreen : Window
    {
        private string username;
        private Communicator communicator = new Communicator();
        private Thread updateThread;
        private List<dynamic> rooms = new List<dynamic>();

        public JoinRoomScreen(string name)
        {
            InitializeComponent();

            TextBlockUser.Text = name;
            username = name;

            updateThread = new Thread(UpdateRoomList);
            updateThread.Start();
        }

     
        private void UpdateRoomList()
        {
            byte[] msg = Serializer.SerializerCodeRequest(PacketCode.GET_ROOMS_REQUEST);
            byte[] res = { 0 };

            while (true)
            {
                rooms.Clear();

                this.Dispatcher.Invoke(() =>
                {
                    RoomsListBox.ClearValue(ItemsControl.ItemsSourceProperty);
                    RoomsListBox.Items.Clear();
                });

                try
                {
                    communicator.sendMsgToServer(msg);
                    res = communicator.getMsgFromServer();
                }
                catch (ThreadAbortException ex)
                {
                    throw ex;
                }
                catch (Exception)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        TextBlockError.Text = "Server has crashed";
                    });
                    return;
                }

                if (Convert.ToInt32(res[0]) == Convert.ToInt32(PacketCode.GET_ROOMS_RESPONSE))
                {
                    GetRoomResponse getRoomResponse = Deserializer.deserializeResponse<GetRoomResponse>(res);

                    if (getRoomResponse.status == Deserializer.failureStatus || getRoomResponse.rooms == "")
                    {
                        Thread.Sleep(3000);
                        continue;
                    }

                    var objects = JArray.Parse(getRoomResponse.rooms); // make list of json 

                    foreach (JObject curRoomData in objects)   // going through each object (room data) in the list
                    {
                        dynamic newRoomData = JsonConvert.DeserializeObject(curRoomData.ToString());
                        rooms.Add(newRoomData);
                    }
          
                    this.Dispatcher.Invoke(() =>   // adding rooms list to GUI
                    {
                        RoomsListBox.ItemsSource = rooms;
                        RoomsListBox.DisplayMemberPath = "name";  // Displaying only the room name
                    });

                }
                else
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        TextBlockError.Text = "Could not update rooms";
                    });
                }
                Thread.Sleep(3000);
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void ButtonJoin_Click(object sender, RoutedEventArgs e)
        {
            if (RoomsListBox.SelectedIndex == -1){ // if chose nothing 
                return;
            }

            dynamic roomData = RoomsListBox.SelectedItem;

            if (roomData.isActive == 0)
                return;


            JoinRoomRequest joinRoomRequest = new JoinRoomRequest { roomid = roomData.id };
            Byte[] res;

            try
            {
                communicator.sendMsgToServer(Serializer.SerializerJoinRoomRequest(joinRoomRequest));
                res = communicator.getMsgFromServer();
            }
            catch (Exception)
            {
                TextBlockError.Text = "Server has crashed";
                return;
            }

            if (Convert.ToInt32(res[0]) == Convert.ToInt32(PacketCode.JOIN_ROOM_RESPONSE))
            {
                JoinRoomResponse joinRoomResponse = Deserializer.deserializeResponse<JoinRoomResponse>(res);

                if (joinRoomResponse.status == Deserializer.failureStatus)
                {
                    TextBlockError.Text = "Could not join the room";
                    return;
                }
                else if (joinRoomResponse.status == Deserializer.successStatus)
                {
                    try
                    {
                        updateThread.Abort();
                    }
                    catch (Exception) { }

                    RoomData data = new RoomData { id = roomData.id, isActive = 1, maxPlayers = roomData.maxPlayers, name = roomData.name, numOfQuestionsInGame = roomData.numOfQuestionsInGame, timePerQuestion = roomData.timePerQuestion };
                    RoomScreen roomScreen = new RoomScreen(username, data, false);
                    roomScreen.Show();
                    this.Close();
                }
            }
        }


        public void BackButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                updateThread.Abort();
            }
            catch (Exception) { }

            MenuScreen menuScreen = new MenuScreen(username);
            menuScreen.Show();
            this.Close();
        }

    }
}
