﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace TriviaWPF
{
    public struct RoomData
    {
        public int id;
        public string name;
        public int maxPlayers;
        public int numOfQuestionsInGame;
        public int timePerQuestion;
        public int isActive;
    }

    struct ErrorResponse
    {
        public string message;
    }

    struct LoginResponse
    {
        public int status;
    }

    struct SignupResponse
    {
        public int status;
    }

    struct LogoutResponse
    {
        public int status;
    }

    struct GetPlayersInRoomResponse
    {
        public List<string> players;
    }

    struct GetHighScoreResponse
    {
        public int status;
        public string UserStatistics;
    }

    struct CreateRoomResponse
    {
        public int status;
    }
    struct GetRoomResponse
    {
        public int status;
        public string rooms;
    }
    
    struct GetPersonalStatsResponse
    {
        public int status;
        public string UserStatistics;
    }

    struct JoinRoomResponse
    {
        public int status;
    }

    struct CloseRoomRespone
    {
        public int status;
    }

    struct StartGameResponse
    {
        public int status;
    }
    struct userStats
    {

        public string AverageAnswerTime;
        public string NumOfCorrectAnswers;
        public string NumOfTotalAnswers;
        public string NumOfPlayerGames;

    }
    struct GetRoomStateResponse
    {
        public int status;
        public bool hasGameBegun;
        public string players;
        public int answerCount;
        public float answerTimeOut;
    }


    struct LeaveRoomResponse
    {
        public int status;
    };

    struct LeaveGameResponse
    {
        public int status;
    };

    struct PlayerResults
    {
        string username;
        public int correctAnswerCount;
        public int wrongAnswerCount;
        public float averageAnswerTime;
    };

    struct GetGameResultsResponse
    {
        public int status;
        public string results;

    };
    struct SubmitAnswerResponse
    {
        public int status;
        public int correctAnswerId;
    };
    struct GetQuestionReponse
    {
        public int status;
        public string question;
        public string answers;
    };
    struct Question
    {
        public int id;
        public string answer;
    };

    public class Deserializer
    {
        public const int failureStatus = 0;
        public const int successStatus = 1;
        public const int alreadyLogged = 3;

        public static T deserializeResponse<T>(Byte[] data)
        {
            string s = System.Text.Encoding.UTF8.GetString(data, 0, data.Length);
            s = s.Remove(0, 5);
            return JsonConvert.DeserializeObject<T>(s);
        }
    }
}
