﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaWPF
{
    /// <summary>
    /// Interaction logic for SignupScreen.xaml
    /// </summary>
    public partial class SignupScreen : Window
    {
        Communicator communicator = new Communicator();

        public SignupScreen()
        {
            InitializeComponent();
            TextBlockError.Text = "";
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow(true);
            mainWindow.Show();
            this.Close();
        }

        private void SubmitSignupButton_Click(object sender, RoutedEventArgs e)
        {
            Byte[] msg = { 0 };
            string name = TextBoxUsername.Text;
            string password = TextBoxPassword.Password;
            string email = TextBoxEmail.Text;

            if (name.Length == 0 || password.Length == 0 || email.Length == 0)
            {
                TextBlockError.Text = "Insert username, password and email";
                return;
            }


            SignupRequest signupReqest = new SignupRequest { username = name, password = password, email = email };
            try // Signing up
            {
                communicator.sendMsgToServer(Serializer.SerializerSignUpRequest(signupReqest));
                msg = communicator.getMsgFromServer();
            }
            catch (Exception)
            {
                TextBlockError.Text = "Server has crashed";
            }

            if (Convert.ToInt32(msg[0]) == Convert.ToInt32(PacketCode.SIGNUP_RESPONSE))
            {
                SignupResponse signupResponse = Deserializer.deserializeResponse<SignupResponse>(msg);
                if (signupResponse.status == Deserializer.failureStatus)
                {
                    TextBlockError.Text = "User already exists.";
                    return;
                }
                else if (signupResponse.status == Deserializer.successStatus)
                {
                    MenuScreen menuScreen = new MenuScreen(name);
                    menuScreen.Show();
                    this.Close();
                }
            }
            else
            {
                ErrorResponse errorResponse = Deserializer.deserializeResponse<ErrorResponse>(msg);
                TextBlockError.Text = errorResponse.message;
            }
        }
    }
}
