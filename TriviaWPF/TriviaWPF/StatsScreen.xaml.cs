﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace TriviaWPF
{
    /// <summary>
    /// Interaction logic for StatsScreen.xaml
    /// </summary>
    public partial class StatsScreen : Window
    {
        private string userName;

        public StatsScreen(string name)
        {
            InitializeComponent();

            userName = name;
            TextBlockUser.Text = name;
            updateStats();
        }

        private void updateStats()
        {
            Byte[] msg = { 0 };
            Communicator communicator = new Communicator();
            try
            {
                communicator.sendMsgToServer(Serializer.SerializerCodeRequest(PacketCode.GET_PERSONAL_STATS_REQUEST));
                msg = communicator.getMsgFromServer();
            }
            catch (Exception)
            {
                TextBlockErrorMsg.Text = "Server has crashed";
                return;
            }

            if (Convert.ToInt32(msg[0]) == Convert.ToInt32(PacketCode.GET_PERSONAL_STATS_RESPONSE))
            {
                GetPersonalStatsResponse getPersonalStatsResponse = Deserializer.deserializeResponse<GetPersonalStatsResponse>(msg);

                if (getPersonalStatsResponse.status == Deserializer.failureStatus)
                {
                    TextBlockErrorMsg.Text = "Error cant get the data";
                    return;
                }
                else if (getPersonalStatsResponse.status == Deserializer.successStatus) 
                {   // Updating Statistics in GUI
                    userStats userStats = JsonConvert.DeserializeObject<userStats>(getPersonalStatsResponse.UserStatistics);
                    TextBlockAverageTimeForAnswer.Text += userStats.AverageAnswerTime;
                    TextBlockNumberOfgames.Text += userStats.NumOfPlayerGames;
                    TextBlockNumberOfRightAnswers.Text += userStats.NumOfCorrectAnswers;
                    TextBlockNumberOfTotalAnswers.Text += userStats.NumOfTotalAnswers;
                }
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void BackButton_click(object sender, RoutedEventArgs e)
        {
            MenuScreen menuScreen = new MenuScreen(userName);
            menuScreen.Show();
            this.Close();
        }
    }
}
