#pragma once

#include "IDatabase.h"
#include "Game.h"
#include "Room.h"

class GameManager
{
public:
	GameManager();

	GameManager(IDatabase* database);
	Game createGame(Room room, std::string time);
	void deleteGame(Game game, std::string time);
	Game& getGame(LoggedUser user);
	void setIDatabase(IDatabase* database);

private:
	IDatabase* m_database;
	std::vector<Game> m_games;

};