#include "LoginManager.h"

bool LoginManager::signup(std::string name, std::string password, std::string email)
{
	if (!m_database->doesUserExist(name))
	{
		m_database->addNewUser(name, password, email);
		login(name, password);
		return true;
	}
	return false;
}

int LoginManager::login(std::string name, std::string password)
{
	if (m_database->doesUserExist(name) && m_database->dosePasswordMatch(m_database->getPassword(name) , password) )
	{
		LoggedUser loggedUser(name);
		if (std::find(m_loggedUsers.begin(), m_loggedUsers.end(), loggedUser) != m_loggedUsers.end()) {
			return ALREADY_LOGGED;
		}
		m_loggedUsers.push_back(loggedUser);
		return 1;
	}
	return 0;
}

void LoginManager::logout(std::string name)
{
	LoggedUser loggedUser(name);

	// Erasing logged user from vector
	std::vector<LoggedUser>::iterator position = std::find(m_loggedUsers.begin(), m_loggedUsers.end(), loggedUser);
	if (position != m_loggedUsers.end())
		m_loggedUsers.erase(position);
}

void LoginManager::setIDatabase(IDatabase* db)
{
	m_database = db;
}
