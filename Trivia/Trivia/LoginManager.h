#pragma once
#include "IDatabase.h"
#include "LoggedUser.h"
#include <algorithm>
#include <vector>

using std::string;
#define ALREADY_LOGGED 3
class LoginManager
{
public:
	bool signup(std::string name, std::string password, std::string email);
	int login(std::string name, std::string password);
	void logout(std::string name);
	void setIDatabase(IDatabase* db);

private:
	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUsers;
};