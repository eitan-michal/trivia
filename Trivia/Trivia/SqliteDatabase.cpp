#include "SqliteDatabase.h"

SqliteDatabase::SqliteDatabase(std::string fileName)
{
	int doesFileExist = _access(fileName.c_str(), 0);
	int res = sqlite3_open(fileName.c_str(), &_db);
	if (res != SQLITE_OK) {
		_db = nullptr;
		throw MyException("Cant open db");
	}

	if (doesFileExist != 0) { // if file doesn't exist
		// init database
		char* errMessage = nullptr;
		const char* sqlStatement;

		// Users table
		sqlStatement = "CREATE TABLE Users("
			"USERNAME TEXT NOT NULL,"
			"PASSWORD TEXT NOT NULL,"
			"EMAIL TEXT NOT NULL);";
		res = sqlite3_exec(_db, sqlStatement, nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
			throw MyException("Cant create users table");


		sqlStatement = "CREATE TABLE games(game_id integer primary key autoincrement not null, status integer not null, start_time DATETIME not null, end_time DATETIME);";
		res = sqlite3_exec(_db, sqlStatement, nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
			throw MyException("Cant create games table");

		sqlStatement = "CREATE TABLE players_answers(game_id integer not null, username text not null, question_id integer not null, player_answer text not null, is_correct integer not null, answer_time integer not null, "
			"primary key(game_id, username, question_id),"
			"foreign key(game_id) REFERENCES games(game_id),"
			"foreign key(username) REFERENCES users(username),"
			"foreign key(question_id) REFERENCES questions(question_id));";
		res = sqlite3_exec(_db, sqlStatement, nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
			throw MyException("Cant create player_answers table");
	}
}

SqliteDatabase::~SqliteDatabase()
{
	sqlite3_close(_db);
	_db = nullptr;
}

bool SqliteDatabase::doesUserExist(std::string userName)
{
	Records records = getRecords("SELECT * FROM USERS WHERE USERNAME LIKE '" + userName + "';", _db);

	return !records.empty();
}

bool SqliteDatabase::dosePasswordMatch(std::string passwordA, std::string passwordB)
{
    return (passwordA == passwordB);  // B like Beyonce
}

std::string SqliteDatabase::getPassword(std::string userName)
{
	Records records = getRecords("SELECT passWord FROM USERS WHERE USERNAME LIKE '" + userName + "';", _db);
	return records[0][0];
}

void SqliteDatabase::addNewUser(std::string name, std::string password, std::string email)
{
	updateRecords("insert into users (username, password, email) values ('" + name + "', '" + password + "', '" + email + "');", _db);
}

float SqliteDatabase::getPlayerAverageAnswerTime(std::string userName) {
	Records records = getRecords("SELECT answer_time FROM players_answers WHERE USERNAME LIKE '" + userName + "';", _db);
	int sum = 0;
	for (Record time : records) {
		sum += stoi(time[0]);
	}
	
	return sum == 0 ? sum : float(sum / records.size());//if played 0 time return 0 else return average time
}

int SqliteDatabase::getNumOfCorrectAnswers(std::string userName)
{
	Records records = getRecords("SELECT COUNT() FROM players_answers WHERE USERNAME LIKE '" + userName + "' AND is_correct == 1;", _db);
	return stoi(records[0][0]);
}

int SqliteDatabase::getNumOfTotalAnswers(std::string userName)
{
	Records records = getRecords("SELECT COUNT() FROM players_answers WHERE USERNAME LIKE '" + userName + "';", _db);
	return stoi(records[0][0]);
}

int SqliteDatabase::getNumOfPlayerGames(std::string userName)
{
	Records records = getRecords("SELECT DISTINCT game_id FROM players_answers WHERE USERNAME LIKE '" + userName + "';", _db);
	return records.size();
}

nlohmann::json SqliteDatabase::getTopUsers()
{
	Records records = getRecords("SELECT username, count(is_correct) FROM players_answers where is_correct == 1 GROUP BY username order by count(is_correct) DESC LIMIT 3;", _db);

	nlohmann::json highScores = nlohmann::json::array();
	for (Record rec : records)
	{
		nlohmann::json score;
		score["name"] = rec[0];
		score["score"] = stoi(rec[1]);
		highScores.push_back(score);
	}
	return highScores;
}

int SqliteDatabase::createGame(std::string time)
{
	updateRecords("insert into games (status, start_time) values(1, '" + time + "');" , _db);
	Records records = getRecords("SELECT game_id FROM games where start_time LIKE '" + time + "';", _db);

	return stoi(records[0][0]);
}

void SqliteDatabase::endGame(std::string time, int id)
{
	updateRecords("UPDATE games SET status = 0, end_time = '" + time + "' where game_id = " + std::to_string(id) + ";", _db);
}

void SqliteDatabase::addUserAnswer(int gameId, int questionId, std::string userName, std::string answer, bool isCorrect, int answerTime)
{
	std::string query = "insert into players_answers (game_id, username, question_id, player_answer, is_correct, answer_time)"
		"values(" + std::to_string(gameId) + ",'" + userName + "'," + std::to_string(questionId) + ",'" + answer + "'," + std::to_string((int)isCorrect) + "," + std::to_string(answerTime) + ");";
	updateRecords(query , _db);
}

std::vector<Question> SqliteDatabase::getQuestion(int numOfQuestions)
{
	std::vector<Question> questions;
	Records records = getRecords("select * from questions limit " + std::to_string(numOfQuestions) + ";", _db);
	for (Record rec : records)
	{
		std::vector<std::string> answers;
		answers.push_back(rec[2]);
		answers.push_back(rec[3]);
		answers.push_back(rec[4]);
		answers.push_back(rec[5]);
		Question quest(rec[1], answers, stoi(rec[0]));
		questions.push_back(quest);
	}

	return questions;
}
