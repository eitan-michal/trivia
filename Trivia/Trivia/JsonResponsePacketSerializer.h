#pragma once
#include <iostream>
#include <vector>
#include "PacketCodes.h"
#include "json-master/single_include/nlohmann/json.hpp"
#include "RoomData.h"

#define ERROR_MSG "Error"

using Buffer = std::vector<unsigned char>;


struct ErrorResponse
{
	std::string message;
};

struct LoginResponse
{
	unsigned int status;
};

struct SignupResponse
{
	unsigned int status;
};

struct LogoutResponse
{
	unsigned int status;
};

struct GetPlayersInRoomResponse
{
	std::vector<std::string> players;
};

struct GetPersonalStatsResponse
{
	unsigned int status;
	std::string statistics;
};

struct CreateRoomResponse
{
	unsigned int status;
};
struct GetRoomResponse {
	unsigned int status;
	std::vector<RoomData> rooms;
};

struct GetHighScoreResponse {
	unsigned int status;
	nlohmann::json statistics;
};

struct JoinRoomResponse {
	unsigned int status;
};

struct CloseRoomRespone
{
	unsigned int status;
};

struct StartGameResponse
{
	unsigned int status;
};


struct GetRoomStateResponse
{
	unsigned int status;
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int answerCount;
	float answerTimeOut;
};


struct LeaveRoomResponse {
	unsigned int status;
};


struct LeaveGameResponse
{
	unsigned int status;
};

struct GetQuestionReponse
{
	unsigned int status;
	std::string question;
	std::map<unsigned int, std::string> answers;
};


struct SubmitAnswerResponse
{
	unsigned int status;
	unsigned int correctAnswerId;
};

struct PlayerResults
{
	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
};

struct GetGameResultsResponse
{
	unsigned int status;
	std::vector<PlayerResults> results;

};

std::string getAnswers(std::map<unsigned int, std::string> answers);
std::string getPlayerResultsList(std::vector<PlayerResults> playersResults);
std::string getRoomsData(std::vector<RoomData> rooms);
std::string getList(std::vector<std::string> list);

class JsonResponsePacketSerializer
{
public:
	static Buffer serializeResponse(ErrorResponse errorResponse);
	static Buffer serializeResponse(LoginResponse loginResponse);
	static Buffer serializeResponse(SignupResponse signupResponse);

	static Buffer serializeResponse(LogoutResponse logoutResponse);
	static Buffer serializeResponse(GetRoomResponse getRoomResponse);

	static Buffer serializeResponse(GetPlayersInRoomResponse getPlayersInRoomResponse);

	static Buffer serializeResponse(JoinRoomResponse joinRoomResponse);
	static Buffer serializeResponse(CreateRoomResponse createRoomResponse);

	static Buffer serializeResponse(GetHighScoreResponse getHighScoreResponse);
	static Buffer serializeResponse(GetPersonalStatsResponse getPersonalStatsResponse);
	
	static Buffer serializeResponse(CloseRoomRespone closeRoomRespone);
	static Buffer serializeResponse(StartGameResponse startGameResponse);
	static Buffer serializeResponse(GetRoomStateResponse getRoomStateResponse);
	static Buffer serializeResponse(LeaveRoomResponse leaveRoomResponse);

	static Buffer serializeResponse(LeaveGameResponse leaveGameResponse);
	static Buffer serializeResponse(GetGameResultsResponse getGameResultsResponse);
	static Buffer serializeResponse(SubmitAnswerResponse submitAnswerResponse);
	static Buffer serializeResponse(GetQuestionReponse getQuestionReponse);

};