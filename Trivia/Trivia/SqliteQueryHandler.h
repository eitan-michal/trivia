#pragma once
#include "sqlite3.h"
#include "MyException.h"
#include <io.h>
#include <vector>

using Record = std::vector<std::string>;
using Records = std::vector<Record>;

int callback(void* data, int argc, char** argv, char** azColName);
void updateRecords(std::string query, sqlite3* db);
Records getRecords(std::string query, sqlite3* db);
