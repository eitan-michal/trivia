#pragma once
#include <iostream>
#include<vector>
#include <algorithm>    
#include <ctime>        
#include <cstdlib> 
class Question
{
public:
	Question();
	Question(std::string question, std::vector<std::string> possibleAnswers, int id);

	std::string getQuestion();
	std::vector<std::string> getPossibleAnswers();
	std::string getCorrectAnswer();
	unsigned int getCorrectAnswerIndex();
	int getID();

private:
	std::string m_question;
	std::vector<std::string> m_possibleAnswers;
	unsigned int m_corectAnsIndex;
	int m_id;
};
