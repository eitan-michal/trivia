#pragma once

#include "Communicator.h"
#include "IDatabase.h"
#include "RequestHandlerFactory.h"

class Server
{
private:
	Communicator m_communicator;
	IDatabase* m_IDatabase;
	RequestHandlerFactory m_handlerFactory;

public:
	Server(IDatabase* db);
	void run();

};