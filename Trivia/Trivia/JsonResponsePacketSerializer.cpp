#include "JsonResponsePacketSerializer.h"

std::string getAnswers(std::map<unsigned int, std::string> answers) {
    //make a list of jsons , answers
    if (answers.empty() == true) {
        return "";
    }
    std::string roomsStr = "[";

    for (std::pair<unsigned int, std::string> data : answers)
    {
        nlohmann::json answersJson;
        answersJson["id"] = data.first;
        answersJson["answer"] = data.second;

        roomsStr += answersJson.dump() + ", ";
    }
    roomsStr.pop_back();
    roomsStr.pop_back();
    roomsStr.push_back(']');

    return roomsStr;


}

std::string getPlayerResultsList(std::vector<PlayerResults> playersResults)
{
    if (playersResults.empty() == true) {
        return "";
    }
    std::string roomsStr = "[";
    for (PlayerResults data : playersResults)
    {
        nlohmann::json PlayerResultsJson;
        PlayerResultsJson["username"] = data.username;
        PlayerResultsJson["correctAnswerCount"] = data.correctAnswerCount;
        PlayerResultsJson["wrongAnswerCount"] = data.wrongAnswerCount;
        PlayerResultsJson["averageAnswerTime"] = data.averageAnswerTime;

        roomsStr += PlayerResultsJson.dump() + ", ";
    }
    roomsStr.pop_back();
    roomsStr.pop_back();
    roomsStr.push_back(']');

    return roomsStr;

}

std::string getRoomsData(std::vector<RoomData> rooms)
{
    if (rooms.empty() == true) {
        return "";
    }
    std::string roomsStr = "[";
    for (RoomData data : rooms)
    {
        nlohmann::json roomJson;
        roomJson["id"] = data.id;
        roomJson["name"] = data.name;
        roomJson["maxPlayers"] = data.maxPlayers;
        roomJson["numOfQuestionsInGame"] = data.numOfQuestionsInGame;
        roomJson["timePerQuestion"] = data.timePerQuestion;
        roomJson["isActive"] = data.isActive;

        roomsStr += roomJson.dump() + ", ";
    }
    roomsStr.pop_back();
    roomsStr.pop_back();
    roomsStr.push_back(']');

    return roomsStr;
}

std::string getList(std::vector<std::string> list) {
    if (list.empty() == true) {
        return "";
    }
    std::string str = "[";
    for (std::string item : list)
    {
        str += item + ",";
    }
    str.pop_back();
    str.push_back(']');
    return str;
}

Buffer serializeJson(nlohmann::json content, char code)
{
    Buffer buffer;

    std::string str = content.dump(); // dumping first into a string so that special chars will disappear
    const char* jsonStr = str.c_str();
    int jsonSize = strlen(jsonStr) + 1;

    auto size = buffer.size();
    buffer.resize(size + sizeof(char) + sizeof(int) + jsonSize); // resizing buffer to the size of the messsage
    std::memcpy(&buffer[size], &code, sizeof(char)); // copying msg code (1 byte) to buffer

    size = sizeof(char);
    std::memcpy(&buffer[size], &jsonSize, sizeof(char)); // copying json size (4 bytes) to buffer

    size += sizeof(int);
    std::memcpy(&buffer[size], jsonStr, jsonSize); // copying the json (x bytes) to buffer

    return buffer;
}

Buffer JsonResponsePacketSerializer::serializeResponse(ErrorResponse errorResponse)
{
    nlohmann::json content;
    content["message"] = errorResponse.message;
    return serializeJson(content, (char)ERROR_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(LoginResponse loginResponse)
{
    nlohmann::json content;
    content["status"] = loginResponse.status;

    return serializeJson(content, (char)LOGIN_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(SignupResponse signupResponse)
{
    nlohmann::json content;
    content["status"] = signupResponse.status;

    return serializeJson(content, (char)SIGNUP_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(LogoutResponse logoutResponse)
{
    nlohmann::json content;
    content["status"] = logoutResponse.status;

    return serializeJson(content, (char)LOGOUT_REQUEST);
}

Buffer JsonResponsePacketSerializer::serializeResponse(GetRoomResponse getRoomResponse)
{
    nlohmann::json content;
    content["status"] = getRoomResponse.status;
    content["rooms"] = getRoomsData(getRoomResponse.rooms);

    return serializeJson(content, (char)GET_ROOMS_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse getPlayersInRoomResponse)
{
    nlohmann::json content;
    std::string dictPlayers = "[";
    for (std::string name : getPlayersInRoomResponse.players) {
        dictPlayers += name + ", ";    
    }
    dictPlayers.pop_back();
    dictPlayers[dictPlayers.size()] = ']';//put [ instead of , at the end 
    content["PlayersInRoom"] = dictPlayers;
    return serializeJson(content, (char)GET_PLAYERS_IN_ROOM_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse createRoomResponse)
{
    nlohmann::json content;
    content["status"] = createRoomResponse.status;
    return serializeJson(content, (char)CREATE_ROOM_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse joinRoomResponse)
{
    nlohmann::json content;
    content["status"] = joinRoomResponse.status;
    return serializeJson(content, (char)JOIN_ROOM_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(GetHighScoreResponse getHighScoreResponse)
{
    nlohmann::json content;
    content["status"] = getHighScoreResponse.status;
    content["UserStatistics"] = (getHighScoreResponse.statistics).dump();

    return serializeJson(content, (char)GET_HIGH_SCORE_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(GetPersonalStatsResponse getPersonalStatsResponse) 
{
    nlohmann::json content;
    content["status"] = getPersonalStatsResponse.status;
    content["UserStatistics"] = getPersonalStatsResponse.statistics;
    return serializeJson(content, (char)GET_PERSONAL_STATS_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(CloseRoomRespone closeRoomRespone)
{
    nlohmann::json content;
    content["status"] = closeRoomRespone.status;
    return serializeJson(content, (char)CLOSE_ROOM_RESPONE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(StartGameResponse startGameResponse)
{
    nlohmann::json content;
    content["status"] = startGameResponse.status;
    return serializeJson(content, (char)START_GAME_RESPONSE);
}


Buffer JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse getRoomStateResponse)
{
    nlohmann::json content;
    content["status"] = getRoomStateResponse.status;
    content["hasGameBegun"] = getRoomStateResponse.hasGameBegun;
    content["players"] = getList(getRoomStateResponse.players);
    content["answerCount"] = getRoomStateResponse.answerCount;
    content["answerTimeOut"] = getRoomStateResponse.answerTimeOut;

    return serializeJson(content, (char)GET_ROOM_STATE_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse leaveRoomResponse)
{
    nlohmann::json content;
    content["status"] = leaveRoomResponse.status;
    return serializeJson(content, (char)LEAVE_ROOM_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse leaveGameResponse)
{
    nlohmann::json content;
    content["status"] = leaveGameResponse.status;
    return serializeJson(content, (char)LEAVE_GAME_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse getGameResultsResponse)
{
    
    nlohmann::json content;
    content["status"] = getGameResultsResponse.status;
    content["results"] = getPlayerResultsList(getGameResultsResponse.results);
    return serializeJson(content, (char)GET_GAME_RESULT_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse submitAnswerResponse)
{
    nlohmann::json content;
    content["status"] = submitAnswerResponse.status;
    content["correctAnswerId"] = submitAnswerResponse.correctAnswerId;
    return serializeJson(content, (char)SUBMIT_ANSWER_RESPONSE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(GetQuestionReponse getQuestionReponse)
{
    nlohmann::json content;
    content["status"] = getQuestionReponse.status;
    content["question"] = getQuestionReponse.question;
    content["answers"] = getAnswers(getQuestionReponse.answers);
    return serializeJson(content, (char)GET_QUESTION_RESPONSE);
}
