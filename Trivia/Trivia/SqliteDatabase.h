#pragma once
#include "IDatabase.h"
#include "sqlite3.h"
#include "MyException.h"
#include "SqliteQueryHandler.h"
#include <io.h>
#include <vector>

using Record = std::vector<std::string>;
using Records = std::vector<Record>;

class SqliteDatabase : public IDatabase
{
public:
	SqliteDatabase(std::string fileName);
	~SqliteDatabase();

	bool doesUserExist(std::string userName);
	bool dosePasswordMatch(std::string passwordA, std::string passwordB);
	std::string getPassword(std::string userName);
	void addNewUser(std::string name, std::string password, std::string email);

	float getPlayerAverageAnswerTime(std::string userName);
	int getNumOfCorrectAnswers(std::string userName);
	int  getNumOfTotalAnswers(std::string userName);
	int getNumOfPlayerGames(std::string userName);
	nlohmann::json getTopUsers();

	int createGame(std::string time);
	void endGame(std::string time, int id);
	void addUserAnswer(int gameId, int questionId, std::string userName, std::string answer, bool isCorrect, int answerTime);

	std::vector<Question> getQuestion(int numOfQuestions);


private:
	sqlite3* _db;

};