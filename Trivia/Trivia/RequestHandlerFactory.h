#pragma once
#include "IDatabase.h"
#include "LoggedUser.h"
#include "StatisticsManager.h"
#include "RoomManager.h"
#include "LoginRequestHandler.h"

#include "GameManager.h"


class IDatabase;
class LoginManager;
class LoginRequestHandler;
class RoomManager;
class MenuRequestHandler;
class RoomMemberRequestHandler;
class RoomAdminRequestHandler;

class GameRequestHandler;
class GameManager;

class RequestHandlerFactory 
{
public:
	RequestHandlerFactory();
	LoginRequestHandler* createLoginRequestHandler();
	LoginManager& getLoginManager();
	void setDataBases(IDatabase* db);
	IDatabase* getDataBase();

	MenuRequestHandler* createMenuRequestHandler(std::string name);
	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();
	
	RoomMemberRequestHandler* createRoomMemberRequestHandler(Room room, std::string user);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(Room room, std::string user);
	GameRequestHandler* createGameRequestHandler(std::string user, Game game);
	
	GameManager& getGameManager();

private:
	LoginManager m_loginManager;
	IDatabase* m_database;
	StatisticsManager m_statisticsManager;
	RoomManager m_roomManager;
	GameManager m_gameManager;
};