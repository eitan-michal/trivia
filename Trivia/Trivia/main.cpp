#include "Server.h"
#include "WSAInitializer.h"
#include "SqliteDatabase.h"
#pragma comment(lib, "Ws2_32.lib")

int main()
{
	std::srand(unsigned(std::time(0)));
	SqliteDatabase* db = nullptr;
	try
	{
		WSAInitializer wsaInit;

		db = new SqliteDatabase("MyDB.sqlite");
		Server myServer(db);

		myServer.run();
	}
	catch (std::exception& e)
	{
		std::cout << "Error occurred: " << e.what() << std::endl;
	}

	delete (db);

	system("PAUSE");
	return 0;
}
