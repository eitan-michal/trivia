#pragma once
#include"Question.h"

struct GameData
{
	GameData(Question question);
	GameData();

	Question currentQuestion;
	unsigned int correctAnswerCount = 0;
	unsigned int wrongAnswerCount = 0;
	float averageAnswerTime = 0;
	unsigned int AnswersTime = 0;
};