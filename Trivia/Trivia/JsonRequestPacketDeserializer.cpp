#include "JsonRequestPacketDeserializer.h"

void from_json(const nlohmann::json& j, LoginRequest& logInfo) {
	j.at("username").get_to(logInfo.userName);
	j.at("password").get_to(logInfo.password);
}

void from_json(const nlohmann::json& j, SignupRequest& signInfo) {
	j.at("username").get_to(signInfo.userName);
	j.at("password").get_to(signInfo.password);
	j.at("email").get_to(signInfo.email);
}

void from_json(const nlohmann::json& j, GetPlayersInRoomRequest& Info) {
	j.at("roomid").get_to(Info.roomid);
}

void from_json(const nlohmann::json& j, JoinRoomRequest& Info) {
	j.at("roomid").get_to(Info.roomid);
}

void from_json(const nlohmann::json& j, CreateRoomRequest& info) {
	j.at("roomName").get_to(info.roomName);
	j.at("maxUsers").get_to(info.maxUsers);
	j.at("qustionCount").get_to(info.qustionCount);
	j.at("answerTimeout").get_to(info.answerTimeout);
}

void from_json(const nlohmann::json& j, SubmitAnswerRequest& info) {
	j.at("answerId").get_to(info.answerId);
	j.at("time").get_to(info.time);
}


LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::vector<unsigned char> buffer){

	LoginRequest loginRequstInfo;
	std::string BuffInfo;
	for (unsigned int i = CODE_LEN; i < buffer.size(); i++) {  // Reading buffer into a string
		BuffInfo += buffer[i];
	}
	nlohmann::json jsonInfo = nlohmann::json::parse(BuffInfo);  // Parsing string to json format
	nlohmann::from_json(jsonInfo, loginRequstInfo); // writing json to a struct
	return loginRequstInfo;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignUpRequest(std::vector<unsigned char> buffer)
{
	SignupRequest SignupRequestInfo;
	std::string BuffInfo;
	for (unsigned int i = CODE_LEN; i < buffer.size(); i++) {// Reading buffer into a string
		BuffInfo += buffer[i];
	}
	nlohmann::json jsonInfo = nlohmann::json::parse(BuffInfo);  // Parsing string to json format
	nlohmann::from_json(jsonInfo, SignupRequestInfo); // writing json to a struct
	return SignupRequestInfo;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(std::vector<unsigned char> buffer)
{
	GetPlayersInRoomRequest PlayersInRoomInfo;
	std::string BuffInfo;
	for (unsigned int i = CODE_LEN; i < buffer.size(); i++) {  // Reading buffer into a string
		BuffInfo += buffer[i];
	}
	nlohmann::json jsonInfo = nlohmann::json::parse(BuffInfo);  // Parsing string to json format
	nlohmann::from_json(jsonInfo, PlayersInRoomInfo); // writing json to a struct
	return PlayersInRoomInfo;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::vector<unsigned char> buffer)
{
	JoinRoomRequest JoinRoomInfo;
	std::string BuffInfo;
	for (unsigned int i = CODE_LEN; i < buffer.size(); i++) {  // Reading buffer into a string
		BuffInfo += buffer[i];
	}
	nlohmann::json jsonInfo = nlohmann::json::parse(BuffInfo);  // Parsing string to json format
	nlohmann::from_json(jsonInfo, JoinRoomInfo); // writing json to a struct
	return JoinRoomInfo;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::vector<unsigned char> buffer)
{
	CreateRoomRequest CreateRoomInfo;
	std::string BuffInfo;
	for (unsigned int i = CODE_LEN; i < buffer.size(); i++) {  // Reading buffer into a string
		BuffInfo += buffer[i];
	}
	nlohmann::json jsonInfo = nlohmann::json::parse(BuffInfo);  // Parsing string to json format
	nlohmann::from_json(jsonInfo, CreateRoomInfo); // writing json to a struct
	return CreateRoomInfo;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(std::vector<unsigned char> buffer)
{
	SubmitAnswerRequest SubmitAnswerInfo;
	std::string BuffInfo;
	for (unsigned int i = CODE_LEN; i < buffer.size(); i++) {  // Reading buffer into a string
		BuffInfo += buffer[i];
	}
	nlohmann::json jsonInfo = nlohmann::json::parse(BuffInfo);  // Parsing string to json format
	nlohmann::from_json(jsonInfo, SubmitAnswerInfo); // writing json to a struct
	return SubmitAnswerInfo;
}
