#include "RequestHandlerFactory.h"
#include "RoomMemberRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include"GameRequestHandler.h"

RequestHandlerFactory::RequestHandlerFactory() {
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
    return new LoginRequestHandler(this);
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
    return m_loginManager;
}

void RequestHandlerFactory::setDataBases(IDatabase* db)
{
    m_database = db;
    m_loginManager.setIDatabase(db);
    m_gameManager.setIDatabase(db);
}

IDatabase* RequestHandlerFactory::getDataBase()
{
    return m_database;
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(std::string name)
{
    return new MenuRequestHandler(this, name);
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
    return m_statisticsManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
    return m_roomManager;
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(Room room, std::string user)
{
    return new RoomMemberRequestHandler(room , user ,  this);
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(Room room, std::string user)
{
    return new RoomAdminRequestHandler(room, user, this);
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(std::string user, Game game)
{
    return new GameRequestHandler(this, user, game);
}

GameManager& RequestHandlerFactory::getGameManager()
{
    return m_gameManager;
}

