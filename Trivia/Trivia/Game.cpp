#include "Game.h"


Game::Game(std::vector<Question> questions, std::vector<LoggedUser> players, IDatabase* database, int id) :
	m_questions(questions), m_database(database), m_id(id)
{		
	GameData gameData(m_questions[0]);

	for (LoggedUser user : players) {
		m_players[user] = gameData;
	}
}

void Game::removePlayer(LoggedUser user)
{
	m_players.erase(user);
}

void Game::submitAnswer(LoggedUser user, int ans, int time)
{

	bool isCorrect = m_players[user].currentQuestion.getCorrectAnswerIndex() == (ans - 1);
	if (isCorrect) {
		m_players[user].correctAnswerCount++;
	}
	else {
		m_players[user].wrongAnswerCount++;
	}
	if (ans > m_players[user].currentQuestion.getPossibleAnswers().size()) {//if did not choose an answer in time
		m_database->addUserAnswer(m_id, m_players[user].currentQuestion.getID(), user.getUsername(), " ", isCorrect, time);
	}
	else
	{
		m_database->addUserAnswer(m_id, m_players[user].currentQuestion.getID(), user.getUsername(), m_players[user].currentQuestion.getPossibleAnswers()[ans - 1], isCorrect, time);
	}
	m_players[user].AnswersTime += time;
	m_players[user].averageAnswerTime = m_players[user].AnswersTime / (m_players[user].wrongAnswerCount + m_players[user].correctAnswerCount);
	

}

void Game::getQuestionForUser(LoggedUser user)
{
	int answersNum = m_players[user].wrongAnswerCount + m_players[user].correctAnswerCount;
	if (answersNum < m_questions.size()) {//if did not finish the game 
		m_players[user].currentQuestion = m_questions[answersNum];//get user the next question
	}
}

int Game::getId()
{
	return m_id;
}

bool Game::doesUserExist(LoggedUser user)
{
	return m_players.end() != m_players.find(user);
}

Question Game::getUserQuestion(LoggedUser user)
{
	return m_players[user].currentQuestion;
}

std::vector<PlayerResults> Game::getPlayerResults() 
{
	std::vector<PlayerResults> results;
	for (std::pair<LoggedUser, GameData> user : m_players)//run on all the players 
	{
		PlayerResults result{ user.first.getUsername(), user.second.correctAnswerCount, user.second.wrongAnswerCount, user.second.averageAnswerTime };
		results.push_back(result);//save every user result
	}
	return results;
}

bool Game::isEmpty()
{
	return m_players.empty();
}



bool Game::operator ==(const Game& other) const 
{
	return this->m_id == other.m_id;
}
