#pragma once
#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RequestHandlerFactory.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "MenuRequestHandler.h"

class RequestHandlerFactory;

#define LOGIN_SUCCESS 1
#define LOGIN_FAILED 0

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(RequestHandlerFactory* handlerFactory);
	virtual bool isRequestRelevant(RequestInfo info);
	virtual RequestResult handleRequest(RequestInfo info);
	virtual void abortUser();


private:
	RequestResult login(RequestInfo);
	RequestResult signup(RequestInfo);
	RequestResult ErrorRes();

	LoginManager& m_loginManager;
	RequestHandlerFactory* m_handlerFactory;
};