#include "Server.h"

Server::Server(IDatabase* db)
{
	m_IDatabase = db;
	m_handlerFactory.setDataBases(db);
	m_communicator.setRequestHandlerFactory(&m_handlerFactory);
}

void Server::run()
{
	std::thread t_connector(&Communicator::startHandleRequests, m_communicator);
	t_connector.detach();

	std::string input = "";
	while (input != "EXIT")
	{
		std::cout << "Enter command: " << std::endl;
		std::cin >> input;
	}
}
