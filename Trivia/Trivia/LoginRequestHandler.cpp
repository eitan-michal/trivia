#include "LoginRequestHandler.h"


LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory* handlerFactory) :
	m_handlerFactory(handlerFactory), m_loginManager(m_handlerFactory->getLoginManager())
{

}

bool LoginRequestHandler::isRequestRelevant(RequestInfo info){
	return (info.id == SIGNUP_REQUEST || info.id == LOGIN_REQUEST);	
}


RequestResult LoginRequestHandler::handleRequest(RequestInfo info)
{

	if (info.id == LOGIN_REQUEST)
	{
		return login(info);
	}
	else if (info.id == SIGNUP_REQUEST)
	{
		return signup(info);
	}
	else {
		return ErrorRes();
	}

}

void LoginRequestHandler::abortUser()
{

}

RequestResult LoginRequestHandler::login(RequestInfo info)
{
	RequestResult requestRes;
	LoginRequest loginReq = JsonRequestPacketDeserializer::deserializeLoginRequest(info.buffer);

	int res = m_handlerFactory->getLoginManager().login(loginReq.userName, loginReq.password);
	LoginResponse loginRes{ res };
	requestRes.response = JsonResponsePacketSerializer::serializeResponse(loginRes);

	if (res == LOGIN_SUCCESS) {	// When login was successful
		requestRes.newHandler = m_handlerFactory->createMenuRequestHandler(loginReq.userName);
	}
	else {// ALREADY_LOGGED / user name is not right
		requestRes.newHandler = nullptr;
	}

	return requestRes;
}

RequestResult LoginRequestHandler::signup(RequestInfo info)
{
	RequestResult requestRes;
	SignupRequest signupReq = JsonRequestPacketDeserializer::deserializeSignUpRequest(info.buffer);

	bool res = m_handlerFactory->getLoginManager().signup(signupReq.userName, signupReq.password, signupReq.email);
	if (res == true)
	{
		SignupResponse signupRes{ LOGIN_SUCCESS };
		requestRes.response = JsonResponsePacketSerializer::serializeResponse(signupRes);
		requestRes.newHandler = m_handlerFactory->createMenuRequestHandler(signupReq.userName);
	}
	else {
		SignupResponse signupRes{ LOGIN_FAILED };
		requestRes.response = JsonResponsePacketSerializer::serializeResponse(signupRes);
		requestRes.newHandler = nullptr;
	}

	return requestRes;
}

RequestResult LoginRequestHandler::ErrorRes()
{
	RequestResult ans;
	ErrorResponse statusId = { ERROR_MSG };
	ans.response = JsonResponsePacketSerializer::serializeResponse(statusId);
	ans.newHandler = nullptr;
	return ans;
}

