#include "Question.h"

Question::Question()
{
	m_corectAnsIndex = 0;
	m_possibleAnswers = std::vector<std::string>();
	m_question = "";
}

Question::Question(std::string question, std::vector<std::string> possibleAnswers, int id):
	m_id(id)
{
	std::string corrrectAns = possibleAnswers[0];
	std::random_shuffle(possibleAnswers.begin(), possibleAnswers.end());
	std::vector<std::string>::iterator it = find(possibleAnswers.begin(), possibleAnswers.end(), corrrectAns);

	m_corectAnsIndex = it - possibleAnswers.begin();
	m_possibleAnswers = possibleAnswers;
	m_question = question;

}

std::string Question::getQuestion()
{
	return m_question;
}

std::vector<std::string> Question::getPossibleAnswers()
{
	return m_possibleAnswers;
}

std::string Question::getCorrectAnswer()
{
	return m_possibleAnswers[m_corectAnsIndex];
}

unsigned int Question::getCorrectAnswerIndex()
{
	return m_corectAnsIndex;
}

int Question::getID()
{
	return m_id;
}
