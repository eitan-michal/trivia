#pragma once
#include"RoomData.h"
#include"LoggedUser.h"
#include<vector>

class Room
{
public:
	Room();
	Room(RoomData info);
	void addUser(LoggedUser newUser);
	void removeUser(LoggedUser user);
	std::vector<std::string> getAllUsers();
	std::vector<LoggedUser> getAllLoggedUsers();
	RoomData getRooMData();
	void setActive(bool isActive);
	bool isFull();
	bool isEmpty();

private:
	RoomData m_matadata;
	std::vector<LoggedUser> m_users;

};

