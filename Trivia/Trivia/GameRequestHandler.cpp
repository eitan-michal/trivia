#include"GameRequestHandler.h"

GameRequestHandler::GameRequestHandler(RequestHandlerFactory* handlerFactory, std::string username,Game game ):
	m_gameManager(handlerFactory->getGameManager()), m_handlerFactory(handlerFactory), m_user(LoggedUser(username)),m_game(game)
{
}

bool GameRequestHandler::isRequestRelevant(RequestInfo info)
{
	return (info.id == LEAVE_GAME_REQUEST || info.id == GET_QUESTION_REQUEST ||
		info.id == SUBMIT_ANSWER_REQUEST || info.id == GET_GAME_RESULT_REQUEST);
}

RequestResult GameRequestHandler::handleRequest(RequestInfo info)
{
	switch (info.id)
	{
		case GET_QUESTION_REQUEST:
			return getQuestion(info);
			break;
		case LEAVE_GAME_REQUEST:
			return leaveGame(info);
			break;
		case SUBMIT_ANSWER_REQUEST:
			return submitAnswer(info);
			break;
		case GET_GAME_RESULT_REQUEST:
			return GetGameResults(info);
			break;
		default:
			return errorRes();
	}
}

void GameRequestHandler::abortUser()
{
	Game& game = m_gameManager.getGame(m_user);
	game.removePlayer(m_user);//remove user
	if (game.isEmpty()) {//if no one left in the game delete game
		m_gameManager.deleteGame(game, "");
	}
	m_handlerFactory->getLoginManager().logout(m_user.getUsername());//remove user

}


RequestResult GameRequestHandler::getQuestion(RequestInfo info) 
{
	m_game = m_gameManager.getGame(m_user);
	Question question = m_game.getUserQuestion(m_user);//give the user the next question
	std::vector<std::string> posAnswers = question.getPossibleAnswers();
	std::map<unsigned int, std::string> answers;
	for (int i = 0; i < posAnswers.size(); i++)//create question map  
	{
		answers[i + 1] = posAnswers[i];
	}

	GetQuestionReponse getQuestionReponse{REQUEST_SUCCESS, question.getQuestion(), answers };
	RequestResult requestResult{ JsonResponsePacketSerializer::serializeResponse(getQuestionReponse), nullptr};
	return requestResult;
}



RequestResult GameRequestHandler::leaveGame(RequestInfo info)
{
	Game &game = m_gameManager.getGame(m_user);
	game.removePlayer(m_user);//remove user from game 
	if (game.isEmpty()) {//if no one remind in the game delete game
		m_gameManager.deleteGame(game, info.receivalTime);
	}
	LeaveGameResponse leaveGameResponse{ REQUEST_SUCCESS };
	RequestResult requestResult{ JsonResponsePacketSerializer::serializeResponse(leaveGameResponse), m_handlerFactory->createMenuRequestHandler(m_user.getUsername()) };
	return requestResult;
}

RequestResult GameRequestHandler::submitAnswer(RequestInfo info)
{
	SubmitAnswerRequest submitAnswerRequest = JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(info.buffer);
	m_gameManager.getGame(m_user).submitAnswer(m_user, submitAnswerRequest.answerId, submitAnswerRequest.time);//submit user Answer 

	SubmitAnswerResponse submitAnswerResponse{ REQUEST_SUCCESS ,m_gameManager.getGame(m_user).getUserQuestion(m_user).getCorrectAnswerIndex() };

	RequestResult RequestResult{ JsonResponsePacketSerializer::serializeResponse(submitAnswerResponse),nullptr };
	m_gameManager.getGame(m_user).getQuestionForUser(m_user);	// changing current question
	return RequestResult;

}


RequestResult GameRequestHandler::GetGameResults(RequestInfo info)
{
	GetGameResultsResponse getGameResultsResponse{ REQUEST_SUCCESS, m_gameManager.getGame(m_user).getPlayerResults() };
	RequestResult requestResult{ JsonResponsePacketSerializer::serializeResponse(getGameResultsResponse),nullptr };
	return requestResult;
}

RequestResult GameRequestHandler::errorRes()
{
    RequestResult ans;
    ErrorResponse statusId = { "Error, request isn't available" };
    ans.response = JsonResponsePacketSerializer::serializeResponse(statusId);
    ans.newHandler = nullptr;
    return ans;
}