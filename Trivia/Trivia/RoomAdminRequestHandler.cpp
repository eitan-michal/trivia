#include "RoomAdminRequestHandler.h"
#include "GameRequestHandler.h"


RoomAdminRequestHandler::RoomAdminRequestHandler(Room room, std::string user, RequestHandlerFactory* requestHandlerFactory) :
	m_room(room), m_user(LoggedUser(user)), m_roomManager(requestHandlerFactory->getRoomManager()), m_requestHandlerFactory(requestHandlerFactory)
{
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo info)
{
	if (isRequestRelevant(info) && m_roomManager.doesRoomExist(m_room)) {
		switch (info.id)
		{
		case CLOSE_ROOM_REQUEST:
			return CloseRoom(info);
			break;
		case START_GAME_REQUEST:
			return StartGame(info);
			break;
		case GET_ROOM_STATE_REQUEST:
			return GetRoomState(info);
			break;
		default:
			break;
		}
	}
	else {
		return errorRes();
	}
    
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo info)
{
	return (info.id == CLOSE_ROOM_REQUEST || info.id == START_GAME_REQUEST || info.id == GET_ROOM_STATE_REQUEST);
}

void RoomAdminRequestHandler::abortUser()
{
	m_roomManager.deleteRoom(m_room.getRooMData().id);
	m_requestHandlerFactory->getLoginManager().logout(m_user.getUsername());
}


RequestResult RoomAdminRequestHandler::CloseRoom(RequestInfo info)
{
	CloseRoomRespone closeRoomRespone{ REQUEST_SUCCESS };
	RequestResult requestResult;
	requestResult.newHandler = m_requestHandlerFactory->createMenuRequestHandler(m_user.getUsername());
	requestResult.response = JsonResponsePacketSerializer::serializeResponse(closeRoomRespone);
	m_roomManager.deleteRoom(m_room.getRooMData().id);
    return requestResult;
}

RequestResult RoomAdminRequestHandler::StartGame(RequestInfo info)
{
	StartGameResponse startGameResponse{ REQUEST_SUCCESS };
	Game game = m_requestHandlerFactory->getGameManager().createGame(m_roomManager.getRoom(m_room.getRooMData().id),info.receivalTime );

	RequestResult requestResult{ JsonResponsePacketSerializer::serializeResponse(startGameResponse), m_requestHandlerFactory->createGameRequestHandler(m_user.getUsername(),game) };
	m_roomManager.activateRoom(m_room.getRooMData().id);
	m_room.setActive(false);

	m_roomManager.RemoveUser(m_user);
	if (m_roomManager.getRoom(m_room.getRooMData().id).isEmpty())
	{
		m_roomManager.deleteRoom(m_room.getRooMData().id);
	}

    return requestResult;
}

RequestResult RoomAdminRequestHandler::GetRoomState(RequestInfo info)
{
	RoomData roomData = m_roomManager.getRoom(m_room.getRooMData().id).getRooMData();
	GetRoomStateResponse getRoomStateResponse{ REQUEST_SUCCESS, roomData.isActive,m_roomManager.getRoom(roomData.id).getAllUsers(), roomData.numOfQuestionsInGame, roomData.timePerQuestion };
	RequestResult requestResult{ JsonResponsePacketSerializer::serializeResponse(getRoomStateResponse), nullptr };
    return requestResult;
}


RequestResult RoomAdminRequestHandler::errorRes()
{
    RequestResult ans;
    ErrorResponse statusId = { "Error, request isn't available" };
    ans.response = JsonResponsePacketSerializer::serializeResponse(statusId);
    ans.newHandler = nullptr;
    return ans;
}