#include "Communicator.h"

Communicator::Communicator():
	m_RequestHandlerFactory(new RequestHandlerFactory)
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(m_serverSocket);
	}
	catch (...) {}
}

void Communicator::startHandleRequests()
{
	bindAndListen();

	while (true)
	{
		// the main thread is only accepting clients 
		// and creates a thread for each client
		std::cout << "Waiting for client connection request" << std::endl;

		try
		{
			accept();
		}
		catch (std::exception& e)
		{
			break;
		}
	}
}

void Communicator::setRequestHandlerFactory(RequestHandlerFactory* r)
{
	m_RequestHandlerFactory = r;
}

void Communicator::bindAndListen()
{
	int port = PORT;
	struct sockaddr_in sa = { 0 };

	// Configuring socket type
	std::cout << "Starting..." << std::endl;
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	if (bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	std::cout << "binded" << std::endl;

	// Start listening for incoming requests of clients
	std::cout << "listening..." << std::endl;
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;
}

void Communicator::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET clientSocket = ::accept(m_serverSocket, NULL, NULL);

	if (clientSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	LoginRequestHandler* loginHandler = m_RequestHandlerFactory->createLoginRequestHandler();
	m_clitent[clientSocket] = loginHandler;

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the thread that is created for each client
	std::thread clientThread(&Communicator::handleNewClient, this, clientSocket);
	clientThread.detach();
}

void Communicator::handleNewClient(SOCKET clientSocket)
{
	RequestResult ans;
	int code = 0;
	do
	{
		try {
			std::vector<unsigned char> buffer = (getPartFromSocket(clientSocket, BYTE_SIZE, 0)); // getting the buffer from client

			time_t curTime = time(NULL);
			code = (int)(buffer[0]);

			RequestInfo info{ code,ctime(&curTime),buffer }; // creating an info struct that includes the buffer and receival time
			ans = m_clitent[clientSocket]->handleRequest(info);
			if (ans.newHandler != nullptr) {  // switching to a new handler
				delete m_clitent[clientSocket];
				m_clitent[clientSocket] = ans.newHandler;

			}
			// sending response to client
			std::string msgToClient(ans.response.begin(), ans.response.end());
			sendData(clientSocket, msgToClient);
		}
		catch (std::exception& e) {
			std::cout << e.what() << std::endl;
			m_clitent[clientSocket]->abortUser();
			break;
		}
		
	} while (code != LOGOUT_REQUEST );
	
	std::cout <<  "handleNewClient - done!!!" << std::endl;
}

std::vector<unsigned char> Communicator::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return std::vector<unsigned char>();
	}

	std::vector<unsigned char> buff(1024);
	int res = recv(sc, (char*)&buff[0], buff.size(), 0);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	return buff;
}

// send data to socket
// this is private function
void Communicator::sendData(SOCKET sc, std::string message)
{
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}
