#include "RoomManager.h"

int RoomManager::currId = 0;

void RoomManager::createRoom(LoggedUser creator, RoomData& info)
{
    info.id = ++currId;
    std::pair<unsigned int, Room>room{ currId,Room(info) };
    m_rooms.insert(room);//add room to map 
    m_rooms[currId].addUser(creator);//add to the same room the user 
}

void RoomManager::deleteRoom(unsigned int id)
{
    std::map<unsigned int, Room>::iterator position = m_rooms.find(id);
    if (position != m_rooms.end())
        m_rooms.erase(position);
}

unsigned int RoomManager::getRoomState(unsigned int id)
{
    return m_rooms[id].getRooMData().isActive;
}

std::vector<RoomData> RoomManager::getRooms()
{
    std::vector<RoomData> roomsData;
    for (std::pair<unsigned int, Room> room : m_rooms) {
        roomsData.push_back(room.second.getRooMData());
    }
    return roomsData;
}

void RoomManager::RemoveUser(LoggedUser user) {

    for (std::pair<unsigned int, Room> room : m_rooms) {
        m_rooms[room.first].removeUser(user);
    }
}
Room RoomManager::getRoom(unsigned int id) 
{
    return m_rooms[id];
}

bool RoomManager::doesRoomExist(Room room)
{
    std::map<unsigned int, Room>::iterator position = m_rooms.find(room.getRooMData().id);
    return (position != m_rooms.end());
}

void RoomManager::activateRoom(unsigned int id)
{
    m_rooms[id].setActive(false);
}

void RoomManager::addUser(unsigned int roomId, LoggedUser newUser)
{
    m_rooms[roomId].addUser(newUser);
}

	