#include "GameManager.h"
#include "RoomData.h"

GameManager::GameManager() {

}

GameManager::GameManager(IDatabase* database):
    m_database(database)
{
}

Game GameManager::createGame(Room room, std::string time)
{
    int id = m_database->createGame(time);//create game in the data base and get the game id
    Game game(m_database->getQuestion(room.getRooMData().numOfQuestionsInGame), room.getAllLoggedUsers(), m_database, id);//create game 
    m_games.push_back(game);//add new game in to m_games
    return game;
}

void GameManager::deleteGame(Game game, std::string time)
{
    m_database->endGame(time, game.getId());//change date in data base that game is over  
    //find the game in m_games and delete the game for all users
    std::vector<Game>::iterator position = std::find(m_games.begin(), m_games.end(), game);
    if (position != m_games.end())
        m_games.erase(position);

}

Game& GameManager::getGame(LoggedUser user)
{
    for (int i = 0;i<m_games.size();i++)//run on m_games to find game and returns is 
    {
        if (m_games[i].doesUserExist(user)) {
            return m_games[i];
        }
    }
    Game g;
    return g;
}

void GameManager::setIDatabase(IDatabase* database)
{
    m_database = database;
}
