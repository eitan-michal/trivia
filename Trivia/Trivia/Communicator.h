#pragma once
#pragma warning(disable:4996)

#include "LoginRequestHandler.h"
#include "MyException.h"
#include <iostream>
#include <string>
#include <thread>
#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include <time.h> 

#define PORT 2222
#define BYTE_SIZE 1024

class Communicator
{
public:
	Communicator();
	~Communicator();
	void setRequestHandlerFactory(RequestHandlerFactory* r);
	void startHandleRequests();

private:
	std::map <SOCKET, IRequestHandler*> m_clitent;
	SOCKET m_serverSocket;
	RequestHandlerFactory* m_RequestHandlerFactory;

	void bindAndListen();
	void accept();
	void handleNewClient(SOCKET clientSocket);

	std::vector<unsigned char> getPartFromSocket(SOCKET sc, int bytesNum, int flags);
	void sendData(SOCKET sc, std::string message);
};
