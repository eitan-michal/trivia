#pragma once
#include <vector>
#include <iostream>

struct RequestInfo 
{
	int id;
	char* receivalTime;
	std::vector<unsigned char> buffer;
};