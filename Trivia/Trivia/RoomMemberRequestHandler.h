#pragma once
#include "IRequestHandler.h"
#include "StatisticsManager.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
#include"JsonResponsePacketSerializer.h"
#include"JsonRequestPacketDeserializer.h"


#define REQUEST_SUCCESS 1
#define REQUEST_FAIL 0

class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(Room room, std::string user, RequestHandlerFactory* requestHandlerFactory);
	RequestResult handleRequest(RequestInfo info);
	bool isRequestRelevant(RequestInfo info);
	virtual void abortUser();


private:
	RequestResult LeaveRoom(RequestInfo info);
	RequestResult GetRoomState(RequestInfo info);
	RequestResult errorRes();

	Room m_room;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory* m_requestHandlerFactory;
};

