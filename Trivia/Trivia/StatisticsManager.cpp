#include "StatisticsManager.h"



std::string StatisticsManager::getHighScore()
{
	return m_database->getTopUsers().dump();
}

std::string StatisticsManager::getUserStatistics(std::string name)
{
	nlohmann::json content;
	content["AverageAnswerTime"] = m_database->getPlayerAverageAnswerTime(name);
	content["NumOfCorrectAnswers"] = m_database->getNumOfCorrectAnswers(name);
	content["NumOfTotalAnswers"] = m_database->getNumOfTotalAnswers(name);
	content["NumOfPlayerGames"] = m_database->getNumOfPlayerGames(name);
	return content.dump();
}

void StatisticsManager::setIDatabase(IDatabase* dataBase)
{
	m_database = dataBase;
}
