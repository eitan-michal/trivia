#pragma once
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "json-master/single_include/nlohmann/json.hpp"
#include "Question.h"

class IDatabase 
{
public:
	virtual bool doesUserExist(std::string userName) = 0;
	virtual bool dosePasswordMatch(std::string passwordA, std::string passwordB) = 0;
	virtual void addNewUser(std::string name, std::string password, std::string email) = 0;
	virtual float getPlayerAverageAnswerTime(std::string userName) = 0;
	virtual int getNumOfCorrectAnswers(std::string userName) = 0;
	virtual int  getNumOfTotalAnswers(std::string userName) = 0;
	virtual int getNumOfPlayerGames(std::string userName) = 0;
	virtual nlohmann::json getTopUsers() = 0;
	virtual std::string getPassword(std::string userName) = 0;
	virtual int createGame(std::string time) = 0;
	virtual void endGame(std::string time, int id) = 0;
	virtual void addUserAnswer(int gameId, int questionId, std::string userName, std::string answer, bool isCorrect, int answerTime) = 0;

	virtual std::vector<Question> getQuestion(int numOfQuestions) = 0;
};