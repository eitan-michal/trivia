#pragma once
#include <iostream>
#include <vector>
#include <cstddef>
#include "json-master/single_include/nlohmann/json.hpp"
#include"PacketCodes.h"

#define CODE_LEN 5

struct LoginRequest{
	std::string userName;
	std::string password;
};

struct SignupRequest {
	std::string userName;
	std::string password;
	std::string email;
};

struct GetPlayersInRoomRequest {
	unsigned int roomid;
};
struct  JoinRoomRequest {
	unsigned int roomid;
};
struct CreateRoomRequest {
	std::string roomName;
	unsigned int maxUsers;
	unsigned int qustionCount;
	unsigned int answerTimeout;
};
struct SubmitAnswerRequest {
	unsigned int answerId;
	int time;
};

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest (std::vector<unsigned char> buffer);
	static SignupRequest deserializeSignUpRequest(std::vector<unsigned char> buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(std::vector<unsigned char> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(std::vector<unsigned char> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(std::vector<unsigned char> buffer);
	static SubmitAnswerRequest deserializerSubmitAnswerRequest(std::vector<unsigned char> buffer);

};
