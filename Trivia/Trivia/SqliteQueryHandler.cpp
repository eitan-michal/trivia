#include "SqliteQueryHandler.h"

int callback(void* data, int argc, char** argv, char** azColName)
{
	Records* records = static_cast<Records*>(data);

	try {
		records->emplace_back(argv, argv + argc);
	}
	catch (...) {
		// abort select on failure, don't let exception propogate thru sqlite3 call-stack
		return 1;
	}

	return 0;
}

void updateRecords(std::string query, sqlite3* db)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, (const char*)query.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		throw MyException("Failed to insert record");
}

Records getRecords(std::string query, sqlite3* db)
{
	Records records;
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, (const char*)query.c_str(), callback, &records, &errMessage);
	if (res != SQLITE_OK)
		throw MyException("Failed to find record");

	return records;
}
