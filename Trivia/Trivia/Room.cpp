#include "Room.h"

Room::Room()
{
}

Room::Room(RoomData info):m_matadata(info)
{
}

void Room::addUser(LoggedUser newUser)
{
	m_users.push_back(newUser);
}

void Room::removeUser(LoggedUser newUser)
{
	std::vector<LoggedUser>::iterator position = std::find(m_users.begin(), m_users.end(), newUser);
	if (position != m_users.end())
		m_users.erase(position);
}

std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> usersList;
	for (LoggedUser user: m_users){
		usersList.push_back(user.getUsername());
	}
	return usersList;
}

std::vector<LoggedUser> Room::getAllLoggedUsers()
{
	return m_users;
}

RoomData Room::getRooMData()
{
	return m_matadata;
}

void Room::setActive(bool isActive)
{
	m_matadata.isActive = isActive;
}

bool Room::isFull()
{
	return m_users.size() == m_matadata.maxPlayers;
}

bool Room::isEmpty()
{
	return m_users.empty();
}

