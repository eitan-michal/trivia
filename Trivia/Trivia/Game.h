#pragma once
#include "Question.h"
#include "GameData.h"
#include "LoggedUser.h"
#include "IDatabase.h"
#include<map>
#include"JsonResponsePacketSerializer.h"

class Game {
private:
	std::vector<Question> m_questions;
	std::map<LoggedUser, GameData> m_players;
	IDatabase* m_database;
	int m_id;

public:
	Game() = default;
	Game(std::vector<Question> questions, std::vector<LoggedUser> players, IDatabase* database, int id);
	void removePlayer(LoggedUser user);
	void submitAnswer(LoggedUser user, int ans, int time);
	void getQuestionForUser(LoggedUser user);
	bool doesUserExist(LoggedUser user);
	Question getUserQuestion(LoggedUser user);
	std::vector<PlayerResults> getPlayerResults();
	bool isEmpty();

	int getId();
	bool operator ==(const Game& other) const;


};


