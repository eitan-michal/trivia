#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(Room room, std::string user, RequestHandlerFactory* requestHandlerFactory) :
	m_room(room), m_user(LoggedUser(user)), m_roomManager(requestHandlerFactory->getRoomManager()), m_requestHandlerFactory(requestHandlerFactory)
{
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo info)
{
	switch (info.id)
	{
	case LEAVE_ROOM_REQUEST:
		return LeaveRoom(info);
	case GET_ROOM_STATE_REQUEST:
		return GetRoomState(info);
	default:
		return errorRes();
	}
	
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo info)
{
	return (info.id == LEAVE_ROOM_REQUEST || info.id == GET_ROOM_STATE_REQUEST);
}

void RoomMemberRequestHandler::abortUser()
{
	m_roomManager.RemoveUser(m_user);
	m_room.removeUser(m_user);
	m_requestHandlerFactory->getLoginManager().logout(m_user.getUsername());
}

RequestResult RoomMemberRequestHandler::LeaveRoom(RequestInfo info)
{
	LeaveRoomResponse leaveRoomResponse{ REQUEST_SUCCESS };
	RequestResult requestResult;
	requestResult.newHandler = m_requestHandlerFactory->createMenuRequestHandler(m_user.getUsername());
	requestResult.response = JsonResponsePacketSerializer::serializeResponse(leaveRoomResponse);

	m_roomManager.RemoveUser(m_user);//remove user from all room's lists
	m_room.removeUser(m_user);

    return requestResult;
}

RequestResult RoomMemberRequestHandler::GetRoomState(RequestInfo info)
{
	GetRoomStateResponse getRoomStateResponse;
	RequestResult requestResult;

	if (!m_roomManager.doesRoomExist(m_room)) {
		getRoomStateResponse = { REQUEST_FAIL, 0, std::vector<std::string>(), 0, 0 };
		requestResult = { JsonResponsePacketSerializer::serializeResponse(getRoomStateResponse), m_requestHandlerFactory->createMenuRequestHandler(m_user.getUsername()) };
	}
	else {
		RoomData roomData = m_roomManager.getRoom(m_room.getRooMData().id).getRooMData();
		getRoomStateResponse = { REQUEST_SUCCESS, bool(roomData.isActive),m_roomManager.getRoom(roomData.id).getAllUsers(), roomData.numOfQuestionsInGame, float(roomData.timePerQuestion) };
		if (roomData.isActive == true)
		{
			requestResult = { JsonResponsePacketSerializer::serializeResponse(getRoomStateResponse), nullptr };
		}
		else{//if the room exist but it is not active the game started
			Game game = m_requestHandlerFactory->getGameManager().getGame(m_user);
			requestResult = { JsonResponsePacketSerializer::serializeResponse(getRoomStateResponse), m_requestHandlerFactory->createGameRequestHandler(m_user.getUsername(), game) };

			m_roomManager.RemoveUser(m_user);
			if (m_roomManager.getRoom(m_room.getRooMData().id).isEmpty())//if the last player in the room delete the room
			{
				m_roomManager.deleteRoom(m_room.getRooMData().id);
			}
		}
	}
	return requestResult;
}


RequestResult RoomMemberRequestHandler::errorRes()
{
    RequestResult ans;
    ErrorResponse statusId = { "Error, request isn't available" };
    ans.response = JsonResponsePacketSerializer::serializeResponse(statusId);
    ans.newHandler = nullptr;
    return ans;
}