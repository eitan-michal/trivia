#pragma once
#include"Room.h"
#include<map>

class RoomManager
{
public:
	void createRoom(LoggedUser creator , RoomData& info);
	void deleteRoom(unsigned int id);
	unsigned int getRoomState(unsigned int id);
	std::vector<RoomData> getRooms();

	void RemoveUser(LoggedUser user);
	Room getRoom(unsigned int id);

	bool doesRoomExist(Room room);
	void activateRoom(unsigned int id);
	void addUser(unsigned int roomId, LoggedUser newUser);


private:
	std::map<unsigned int, Room> m_rooms;
	static int currId;
};
