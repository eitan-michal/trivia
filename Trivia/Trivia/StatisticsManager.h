#pragma once
#include "IDatabase.h"
#include "SqliteQueryHandler.h"
#include "SqliteDatabase.h"
#include "json-master/single_include/nlohmann/json.hpp"

class StatisticsManager
{
public:
	std::string getHighScore();
	std::string getUserStatistics(std::string name);
	void setIDatabase(IDatabase* dataBase);

private:
	IDatabase *m_database;
};

