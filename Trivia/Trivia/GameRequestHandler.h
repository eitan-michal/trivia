#pragma once

#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "Game.h"
#include "GameManager.h"

#define REQUEST_SUCCESS 1
#define REQUEST_FAIL 0

class RequestHandlerFactory;

class GameRequestHandler : public IRequestHandler
{
private:
	Game m_game;
	LoggedUser m_user;
	GameManager& m_gameManager;
	RequestHandlerFactory* m_handlerFactory;

	
	RequestResult getQuestion(RequestInfo info);
	RequestResult leaveGame(RequestInfo info);
	RequestResult submitAnswer(RequestInfo info);
	RequestResult GetGameResults(RequestInfo info);
	RequestResult errorRes();

public:
	GameRequestHandler(RequestHandlerFactory* handlerFactory, std::string username, Game game);
	virtual bool isRequestRelevant(RequestInfo info);
	virtual RequestResult handleRequest(RequestInfo info);
	virtual void abortUser();

}; 