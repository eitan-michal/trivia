#include "MenuRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "RoomAdminRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory* requestHandler, std::string userName) :
    m_handlerFactory(requestHandler), m_roomManager(requestHandler->getRoomManager()),
    m_statisticsManager(requestHandler->getStatisticsManager()), m_user(LoggedUser(userName))
{
    m_statisticsManager.setIDatabase(m_handlerFactory->getDataBase());
}


bool MenuRequestHandler::isRequestRelevant(RequestInfo info)
{
    return (info.id == LOGOUT_REQUEST || info.id == GET_ROOM_REQUEST || info.id == GET_PLAYERS_IN_ROOM_REQUEST || info.id == GET_PERSONAL_STATS_REQUEST || info.id == GET_HIGH_SCORE_REQUEST
            || info.id == JOIN_ROOM_REQUEST || info.id == CREATE_ROOM_REQUEST);
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo info)
{
    switch (info.id)
    {
    case LOGOUT_REQUEST:
        return signout(info);
    case GET_ROOMS_REQUEST:
        return getRooms(info);
    case GET_PLAYERS_IN_ROOM_REQUEST:
        return getPlayersInRoom(info);
    case CREATE_ROOM_REQUEST:
        return createRoom(info);
    case GET_PERSONAL_STATS_REQUEST:
        return getPersonalStats(info);
    case GET_HIGH_SCORE_REQUEST:
        return getHighScore(info);
    case JOIN_ROOM_REQUEST:
        return joinRoom(info);
    default:
        return errorRes();
    }
}

RequestResult MenuRequestHandler::signout(RequestInfo info)
{
    m_handlerFactory->getLoginManager().logout(m_user.getUsername());//delete from vector of log users
    m_roomManager.RemoveUser(m_user);//delete from rooms
    LogoutResponse logoutResponse{ REQUEST_SUCCESS };
    RequestResult requestResult{JsonResponsePacketSerializer::serializeResponse(logoutResponse), m_handlerFactory->createLoginRequestHandler()};
    return requestResult;
}

RequestResult MenuRequestHandler::getRooms(RequestInfo info) 
{ 
    GetRoomResponse getRoomResponse{ REQUEST_SUCCESS, m_roomManager.getRooms() };
    RequestResult requestResult{ JsonResponsePacketSerializer ::serializeResponse(getRoomResponse), nullptr};
    return requestResult;
};

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo info)
{
    GetPlayersInRoomRequest getPlayersInRoomRequest = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(info.buffer);
    GetPlayersInRoomResponse getPlayersInRoomResponse{ m_roomManager.getRoom(getPlayersInRoomRequest.roomid).getAllUsers() };
    RequestResult requestResult{ JsonResponsePacketSerializer::serializeResponse(getPlayersInRoomResponse), nullptr };
    return requestResult;
};


RequestResult MenuRequestHandler::getPersonalStats(RequestInfo info) { 
    GetPersonalStatsResponse getPersonalStatsResponse{ REQUEST_SUCCESS , m_statisticsManager.getUserStatistics(m_user.getUsername()) };
    RequestResult requestResult{ JsonResponsePacketSerializer::serializeResponse(getPersonalStatsResponse), nullptr };
    return requestResult;
};

RequestResult MenuRequestHandler::getHighScore(RequestInfo info) {
    GetHighScoreResponse getHighScoreResponse{ REQUEST_SUCCESS ,m_statisticsManager.getHighScore() };
    RequestResult requestResult{ JsonResponsePacketSerializer::serializeResponse(getHighScoreResponse), nullptr };
    return requestResult;
};

RequestResult MenuRequestHandler::joinRoom(RequestInfo info)
{
    JoinRoomRequest joinRoomRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(info.buffer);
    Room room  = m_roomManager.getRoom(joinRoomRequest.roomid);
    RequestResult requestResult;

    if (!room.isFull())  // Joining room only when not full
    {
        room.addUser(m_user);
        m_roomManager.addUser(room.getRooMData().id, m_user);
        JoinRoomResponse joinRoomResponse{ REQUEST_SUCCESS };
        requestResult.response = JsonResponsePacketSerializer::serializeResponse(joinRoomResponse);
        requestResult.newHandler = m_handlerFactory->createRoomMemberRequestHandler(room ,m_user.getUsername());
    }
    else {
        JoinRoomResponse joinRoomResponse{ REQUEST_FAIL };
        requestResult.response = JsonResponsePacketSerializer::serializeResponse(joinRoomResponse);
        requestResult.newHandler = nullptr;    
    }
    
    return requestResult; 
}

RequestResult MenuRequestHandler::createRoom(RequestInfo info)
{ 
    CreateRoomRequest createRoomRequest = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(info.buffer);
    RoomData roomInfo{0, createRoomRequest.roomName,createRoomRequest.maxUsers, createRoomRequest.qustionCount, createRoomRequest.answerTimeout, 1};//the room id is selected in createRoom function
    m_roomManager.createRoom(m_user , roomInfo);
    CreateRoomResponse createRoomResponse{REQUEST_SUCCESS};
    RequestResult requestResult {JsonResponsePacketSerializer::serializeResponse(createRoomResponse), m_handlerFactory->createRoomAdminRequestHandler(m_roomManager.getRoom(roomInfo.id), m_user.getUsername())};
    return requestResult;
}

void MenuRequestHandler::abortUser()
{
    m_handlerFactory->getLoginManager().logout(m_user.getUsername());//delete from vector of log users
    m_roomManager.RemoveUser(m_user);//delete from rooms
}


RequestResult MenuRequestHandler::errorRes()
{
    RequestResult ans;
    ErrorResponse statusId = { "Error, request isn't available" };
    ans.response = JsonResponsePacketSerializer::serializeResponse(statusId);
    ans.newHandler = nullptr;
    return ans;
}